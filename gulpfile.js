'use strict';

var gulp = require('gulp');
var concat = require("gulp-concat");
var gzip = require('gulp-gzip');
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var uglify = require('gulp-uglify');

var VENDOR_SRC_PATH = 'node_modules';
var VENDOR_DEST_PATH = 'src/main/resources/static/';

// Set the browser that you want to support
var AUTOPREFIXER_BROWSERS = [
    'ie >= 10',
    'ie_mob >= 10',
    'ff >= 30',
    'chrome >= 34',
    'safari >= 7',
    'opera >= 23',
    'ios >= 7',
    'android >= 4.4',
    'bb >= 10'
];

gulp.task('minify-css', function(){

    // vendor CSS
    return gulp.src([
        VENDOR_SRC_PATH + '/bootstrap/dist/css/bootstrap.css',
        VENDOR_SRC_PATH + '/summernote/dist/summernote-bs4.css'
    ])
        .pipe(concat('vendor.css'))
        .pipe(autoprefixer({browsers: AUTOPREFIXER_BROWSERS}))
        .pipe(csso())
        .pipe(gzip())
        .pipe(gulp.dest(VENDOR_DEST_PATH + '/css'));
});


gulp.task('vendor-fonts', function() {

    // vendor fonts
    return gulp.src(VENDOR_SRC_PATH + '/summernote/dist/font/*')
        .pipe(gulp.dest(VENDOR_DEST_PATH + '/css/font'));
});


gulp.task('vendor-imgs', function() {

    // vendor imgs
    return gulp.src([ VENDOR_SRC_PATH + '/feather-icons/dist/icons/*' ])
        .pipe(gulp.dest(VENDOR_DEST_PATH + '/icons'));
});

// TODO: jquery has to be alone. I don't know why
gulp.task('vendor-jquery', function() {

    return gulp.src(VENDOR_SRC_PATH + '/jquery/dist/jquery.js')
        .pipe(uglify())
        .pipe(gzip())
        .pipe(gulp.dest(VENDOR_DEST_PATH + '/js'));

});


gulp.task('vendor-js', function() {

        // vendor js
        return gulp.src([
            //  VENDOR_SRC_PATH + '/jquery/dist/jquery.js',
            VENDOR_SRC_PATH + '/bootstrap/dist/js/bootstrap.bundle.js',
            VENDOR_SRC_PATH + '/summernote/dist/summernote-bs4.js',
            VENDOR_SRC_PATH + '/feather-icons/dist/feather.js'
        ])
            .pipe(concat('vendor.js'))
            .pipe(uglify())
            .pipe(gzip())
            .pipe(gulp.dest(VENDOR_DEST_PATH + '/js'));
});

gulp.task('older-ie', function() {

        // for older IE
        return gulp.src([
            VENDOR_SRC_PATH + '/respond.js/dest/respond.min.js',
            VENDOR_SRC_PATH + '/html5shiv/dist/html5shiv.js'
        ])
            .pipe(concat('vendorIE9.js'))
            .pipe(uglify())
            .pipe(gzip())
            .pipe(gulp.dest(VENDOR_DEST_PATH + '/js'));
});


gulp.task('default', gulp.series(
        'minify-css','vendor-fonts','vendor-imgs','vendor-jquery','vendor-js','older-ie', function (done) {
            done();
        })
);

