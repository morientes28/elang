package sk.morione.elango;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;


@SpringBootApplication
@EnableScheduling
@EnableCaching
@ComponentScan("sk.morione.elango")
public class ElangoApplication {

  public static void main(String[] args) {
	SpringApplication.run(ElangoApplication.class, args);
  }

}
