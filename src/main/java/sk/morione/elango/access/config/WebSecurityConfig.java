package sk.morione.elango.access.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.BeanIds;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import sk.morione.elango.access.handler.AuthenticationSuccessHandlerImpl;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 16/10/2018.
 */
@Configuration
@EnableWebSecurity
@Profile(value = {"dev", "stage", "production"})
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {


  @Autowired
  private UserDetailsService userDetailsService;

  @Bean
  public BCryptPasswordEncoder bCryptPasswordEncoder() {
	return new BCryptPasswordEncoder();
  }

  @Bean(name = BeanIds.AUTHENTICATION_MANAGER)
  @Override
  public AuthenticationManager authenticationManagerBean() throws Exception {
	return super.authenticationManagerBean();
  }

  @Bean
  public AuthenticationSuccessHandler authenticationSuccessHandler() {
	return new AuthenticationSuccessHandlerImpl();
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {

	http
			.authorizeRequests()
			.antMatchers(
					"/",
					"/css/**",
					"/img/**",
					"/flags/**",
					"/resources/**",
					"/registration",
					"/actuator/**").permitAll()
			.anyRequest().authenticated()
			.and()
			.formLogin()
			.loginPage("/login")
			.successHandler(authenticationSuccessHandler())
			.permitAll()
			.and()
			.logout()
			.permitAll();

	http.headers()
			.frameOptions()
			.disable()
			.and()
			.csrf()
			.disable();
  }


  @Autowired
  public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {

	auth.userDetailsService(userDetailsService).passwordEncoder(bCryptPasswordEncoder());
  }
}
