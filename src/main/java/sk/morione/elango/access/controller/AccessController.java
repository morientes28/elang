package sk.morione.elango.access.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import sk.morione.elango.access.model.User;
import sk.morione.elango.access.service.SecurityService;
import sk.morione.elango.access.service.UserService;
import sk.morione.elango.access.validator.UserValidator;
import sk.morione.elango.core.controller.BaseController;
import sk.morione.elango.script.model.Language;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 08/10/2018.
 */
@Controller
public class AccessController extends BaseController {

  private final Logger log = LoggerFactory.getLogger(AccessController.class);

  @Autowired
  private UserService userService;

  @Autowired
  private SecurityService securityService;

  @Autowired
  private UserValidator userValidator;

  @GetMapping("/login")
  public String loginForm() {
	return "login";
  }

  @PostMapping("/logout")
  public String logout() {
	return "redirect:login";
  }

  @GetMapping("/registration")
  public String registration(Model model) {

	model.addAttribute("userForm", new User());

	return "registration";
  }

  @PostMapping("/registration")
  public String registration(@ModelAttribute("userForm") User userForm,
							 BindingResult bindingResult) {
	userValidator.validate(userForm, bindingResult);

	if (bindingResult.hasErrors()) {
	  log.info("Registration with errors {}", bindingResult.getFieldErrors());
	  return "registration";
	}

	userService.save(userForm);

	securityService.autoLogin(userForm.getUsername(), userForm.getPasswordConfirm());

	return "redirect:/welcome";
  }

  @GetMapping("/welcome")
  public String welcome() {

	return "welcome";
  }

  @GetMapping("/switch-corpus/{lang}")
  public String switchCorpus(@PathVariable("lang") long lang) {

	securityService.prepareProfile(
			Language.valueOf(lang), activeProfile().getUsername()
	);
	log.debug("Switch corpus to {}", lang);
	return "redirect:/scripts";
  }
}
