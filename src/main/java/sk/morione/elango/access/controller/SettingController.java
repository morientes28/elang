package sk.morione.elango.access.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sk.morione.elango.access.model.Setting;
import sk.morione.elango.access.service.SecurityService;
import sk.morione.elango.access.service.SettingService;
import sk.morione.elango.core.controller.BaseController;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 08/10/2018.
 */
@Controller
public class SettingController extends BaseController {

  private final Logger log = LoggerFactory.getLogger(SettingController.class);

  @Autowired
  private SettingService service;

  @Autowired
  private SecurityService securityService;


  @GetMapping(value = {"/setting"})
  public String setting(Model model) {
	model.addAttribute("setting", activeProfile().getSetting());
	return "setting";
  }

  @PostMapping("/setting")
  public String saveSetting(Setting setting, RedirectAttributes redirectAttributes) {

	service.update(setting);
	securityService.updateProfile(setting, activeProfile());

	redirectAttributes.addFlashAttribute("message",
			"You have been successfully uploaded your settings");

	return "redirect:setting";
  }

}
