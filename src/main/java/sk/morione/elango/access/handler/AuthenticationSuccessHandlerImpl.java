package sk.morione.elango.access.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import sk.morione.elango.access.service.SecurityService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collection;


/**
 * Created by  ⌐⨀_⨀¬ morientes on 26/10/2018.
 */
public class AuthenticationSuccessHandlerImpl implements AuthenticationSuccessHandler {

  private static final Logger log = LoggerFactory.getLogger(AuthenticationSuccessHandlerImpl.class);

  private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

  @Autowired
  private SecurityService securityService;

  @Override
  public void onAuthenticationSuccess(HttpServletRequest request,
									  HttpServletResponse response, Authentication authentication)
		  throws IOException {

	handle(request, response, authentication);
	clearAuthenticationAttributes(request);
  }

  protected void handle(HttpServletRequest request,
						HttpServletResponse response, Authentication authentication)
		  throws IOException {

	String targetUrl = determineTargetUrl(authentication);
	securityService.setUserProfile(authentication);

	if (response.isCommitted()) {
	  log.debug(
			  "Response has already been committed. Unable to redirect to "
					  + targetUrl);
	  return;
	}

	redirectStrategy.sendRedirect(request, response, targetUrl);
  }

  protected String determineTargetUrl(Authentication authentication) {
	boolean isUser = false;
	boolean isAdmin = false;
	Collection<? extends GrantedAuthority> authorities
			= authentication.getAuthorities();
	for (GrantedAuthority grantedAuthority : authorities) {
	  if (grantedAuthority.getAuthority().equals("USER")) {
		isUser = true;
		break;
	  } else if (grantedAuthority.getAuthority().equals("ADMIN")) {
		isAdmin = true;
		break;
	  }
	}

	if (isUser) {
	  log.debug("User has role USER");
	  return "/scripts";
	} else if (isAdmin) {
	  log.debug("User has role ADMIN");
	  return "/import";
	} else {
	  throw new IllegalStateException();
	}
  }

  protected void clearAuthenticationAttributes(HttpServletRequest request) {
	HttpSession session = request.getSession(false);
	if (session == null) {
	  return;
	}
	session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
  }

  protected RedirectStrategy getRedirectStrategy() {
	return redirectStrategy;
  }

  public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
	this.redirectStrategy = redirectStrategy;
  }

}
