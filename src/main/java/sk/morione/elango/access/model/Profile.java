package sk.morione.elango.access.model;

import sk.morione.elango.script.model.Corpus;

import java.io.Serializable;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 08/10/2018.
 */

public class Profile implements Serializable {

  private String username;
  private long userId;
  private Corpus selectedCorpus;
  private Integer points;
  private boolean login;
  private boolean admin;
  private Setting setting;

  public Profile() {
  }

  public Profile(String username, Corpus selectedCorpus, Integer points, long userId) {
	this.username = username;
	this.selectedCorpus = selectedCorpus;
	this.points = points;
	this.login = true;
	this.userId = userId;
  }

  public void clean() {
	this.username = "";
	this.selectedCorpus = new Corpus();
	this.points = 0;
	this.login = false;
	this.userId = 0;
	this.admin = false;
  }

  public String getUsername() {
	return username;
  }

  public void setUsername(String username) {
	this.username = username;
  }

  public Corpus getSelectedCorpus() {
	return selectedCorpus;
  }

  public void setSelectedCorpus(Corpus corpus) {
	this.selectedCorpus = corpus;
  }

  public Integer getPoints() {
	return points;
  }

  public void setPoints(Integer points) {
	this.points = points;
  }

  public boolean isLogin() {
	return login;
  }

  public void setLogin(boolean login) {
	this.login = login;
  }

  public long getUserId() {
	return userId;
  }

  public void setUserId(long userId) {
	this.userId = userId;
  }

  public boolean isAdmin() {
	return admin;
  }

  public void setAdmin(boolean admin) {
	this.admin = admin;
  }

  public Setting getSetting() {
	return setting;
  }

  public void setSetting(Setting setting) {
	this.setting = setting;
  }
}
