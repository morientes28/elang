package sk.morione.elango.access.model;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 26/10/2018.
 */
public enum Role {

  USER, ADMIN

}
