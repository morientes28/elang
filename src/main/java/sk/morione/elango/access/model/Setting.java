package sk.morione.elango.access.model;

import sk.morione.elango.core.model.Base;
import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.model.Language;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * @author ⌐⨀_⨀¬ morientes on 16/11/2018.
 */
@Entity
@SequenceGenerator(name = "seq", allocationSize = 100)
public class Setting extends Base implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
  private Long id;

  private int rowsCount = 10;
  private String sortBy = "id";
  private String sort = "ASC";

  private int rowsCountReview = 10;

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "corpus_id")
  private Corpus corpus; // default corpus

  private Language language = Language.SK;  // translation language

  @OneToOne(fetch = FetchType.EAGER)
  @JoinColumn(name = "user_id")
  private User user;

  public Setting() {

  }

  public Setting(Corpus corpus, User user) {

	this.corpus = corpus;
	this.user = user;
  }

  public int getRowsCount() {
	return rowsCount;
  }

  public void setRowsCount(int rowsCount) {
	this.rowsCount = rowsCount;
  }

  public String getSortBy() {
	return sortBy;
  }

  public void setSortBy(String sortBy) {
	this.sortBy = sortBy;
  }

  public String getSort() {
	return sort;
  }

  public void setSort(String sort) {
	this.sort = sort;
  }

  public int getRowsCountReview() {
	return rowsCountReview;
  }

  public void setRowsCountReview(int rowsCountReview) {
	this.rowsCountReview = rowsCountReview;
  }

  public Corpus getCorpus() {
	return corpus;
  }

  public void setCorpus(Corpus corpus) {
	this.corpus = corpus;
  }

  public Language getLanguage() {
	return language;
  }

  public void setLanguage(Language language) {
	this.language = language;
  }

  public User getUser() {
	return user;
  }

  public void setUser(User user) {
	this.user = user;
  }

  public Long getId() {
	return id;
  }

  public void setId(Long id) {
	this.id = id;
  }

  @Override
  public boolean equals(Object o) {
	if (this == o) return true;
	if (!(o instanceof Setting)) return false;
	Setting setting = (Setting) o;
	return Objects.equals(id, setting.id);
  }

  @Override
  public int hashCode() {

	return Objects.hash(id);
  }

  @Override
  public String toString() {
	return "Setting{" +
			"rowsCount=" + rowsCount +
			", sortBy='" + sortBy + '\'' +
			", sort='" + sort + '\'' +
			", rowsCountReview=" + rowsCountReview +
			", defaultCorpus=" + corpus +
			", translationLanguage=" + language +
			", user=" + user +
			'}';
  }
}
