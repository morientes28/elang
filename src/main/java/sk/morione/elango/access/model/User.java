package sk.morione.elango.access.model;

import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.model.Script;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 26/10/2018.
 */
@Entity
@Table(name = "users")
@SequenceGenerator(name = "seq", allocationSize = 100)
public class User implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
  private Long id;

  private String username;

  private String password;

  @Transient
  private String passwordConfirm;

  private boolean enabled;

  @OneToMany(mappedBy = "user")
  private Set<Corpus> corpuses;

  private Role role;

  @ManyToMany(fetch = FetchType.LAZY,
		  cascade = {
				  CascadeType.PERSIST,
				  CascadeType.MERGE
		  },
		  mappedBy = "users")
  private Set<Script> scripts = new HashSet<>();

  public User() {

  }

  public User(String username, String password, Set<Corpus> corpuses, Role role) {
	this.username = username;
	this.password = password;
	this.corpuses = corpuses;
	this.role = role;
  }

  public Long getId() {
	return id;
  }

  public void setId(Long id) {
	this.id = id;
  }

  public String getUsername() {
	return username;
  }

  public void setUsername(String username) {
	this.username = username;
  }

  public String getPassword() {
	return password;
  }

  public void setPassword(String password) {
	this.password = password;
  }

  public boolean isEnabled() {
	return enabled;
  }

  public void setEnabled(boolean enabled) {
	this.enabled = enabled;
  }

  public Role getRole() {
	return role;
  }

  public void setRole(Role role) {
	this.role = role;
  }

  public String getPasswordConfirm() {
	return passwordConfirm;
  }

  public void setPasswordConfirm(String passwordConfirm) {
	this.passwordConfirm = passwordConfirm;
  }

  public Set<Corpus> getCorpuses() {
	return corpuses;
  }

  public void setCorpuses(Set<Corpus> corpuses) {
	this.corpuses = corpuses;
  }

  public Set<Script> getScripts() {
	return scripts;
  }

  public void setScripts(Set<Script> scripts) {
	this.scripts = scripts;
  }

  public void addScript(Script script) {
	this.getScripts().add(script);
  }

  @Override
  public boolean equals(Object o) {
	if (this == o) return true;
	if (!(o instanceof User)) return false;
	User user = (User) o;
	return enabled == user.enabled &&
			Objects.equals(username, user.username) &&
			Objects.equals(password, user.password);
  }

  @Override
  public int hashCode() {

	return Objects.hash(username, password, enabled);
  }

  @Override
  public String toString() {
	return "User{" +
			"id=" + id +
			", username='" + username + '\'' +
			'}';
  }
}
