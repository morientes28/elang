package sk.morione.elango.access.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sk.morione.elango.access.model.User;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 26/10/2018.
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long> {
  User findByUsername(String username);
}
