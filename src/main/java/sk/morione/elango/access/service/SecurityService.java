package sk.morione.elango.access.service;

import com.hazelcast.core.HazelcastInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import sk.morione.elango.access.controller.AccessController;
import sk.morione.elango.access.model.Profile;
import sk.morione.elango.access.model.Role;
import sk.morione.elango.access.model.Setting;
import sk.morione.elango.access.model.User;
import sk.morione.elango.access.repository.SettingRepository;
import sk.morione.elango.access.repository.UserRepository;
import sk.morione.elango.core.metrics.UserMetrics;
import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.model.Language;
import sk.morione.elango.script.repository.CorpusRepository;

import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 26/10/2018.
 */
@Service
public class SecurityService {

  private final Logger log = LoggerFactory.getLogger(AccessController.class);
  @Autowired(required = false)
  private AuthenticationManager authenticationManager;
  @Autowired
  private UserDetailsService userDetailsService;
  @Autowired
  private CorpusRepository corpusRepository;
  @Autowired
  private UserRepository userRepository;
  @Autowired
  SettingRepository settingRepository;
  @Autowired(required = false)
  private HttpSession session;
  @Autowired(required = false)
  private HazelcastInstance hazelcastInstance;

  public String findLoggedInUsername() {
	Object userDetails = SecurityContextHolder.getContext().getAuthentication().getDetails();
	if (userDetails instanceof UserDetails) {
	  return ((UserDetails) userDetails).getUsername();
	}

	return null;
  }

  public void autoLogin(String username, String password) {
	UserDetails userDetails = userDetailsService.loadUserByUsername(username);
	UsernamePasswordAuthenticationToken usernamePasswordAuthenticationToken =
			new UsernamePasswordAuthenticationToken(
					userDetails,
					password,
					userDetails.getAuthorities()
			);

	Authentication auth = authenticationManager.authenticate(usernamePasswordAuthenticationToken);

	if (usernamePasswordAuthenticationToken.isAuthenticated()) {
	  SecurityContextHolder.getContext().setAuthentication(usernamePasswordAuthenticationToken);

	  setUserProfile(auth);
	  log.debug(String.format("Auto login %s successfully!", username));
	}
  }

  public void setUserProfile(Authentication authentication) {

	Object principal = authentication.getPrincipal();
	if (principal instanceof org.springframework.security.core.userdetails.User) {

	  String username = ((org.springframework.security.core.userdetails.User) principal).getUsername();

	  Profile profile = prepareProfile(Language.EN, username);
	  setSession(profile);
	  log.debug("Setting a user profile {}", profile);
	}

  }

  public Profile prepareProfile(Language language, String username) {

	User user = userRepository.findByUsername(username);
	Corpus actualCorpus = corpusRepository.findOneByUserAndLanguage(user, language);

	if (actualCorpus == null) {
	  actualCorpus = corpusRepository.save(new Corpus(language, user.getUsername(), user));
	  log.info("Created a new corpus {}", actualCorpus);
	}
	Profile profile = new Profile(user.getUsername(), actualCorpus, 1, user.getId());
	profile.setAdmin(user.getRole() == Role.ADMIN);
	profile.setSetting(prepareSetting(profile, user));

	session.setAttribute("profile", profile);
	return profile;
  }

  public void updateProfile(Setting setting, Profile profile) {
	profile.setSetting(setting);
	session.setAttribute("profile", profile);
  }

  private void setSession(Profile profile) {

	// add value to in memory grid - hazelcast
	Map<String, UserMetrics.User> ul = hazelcastInstance.getMap("users");
	ul.put(session.getId(), new UserMetrics.User(profile.getUsername(), session.getId()));
  }

  private Setting prepareSetting(Profile profile, User user) {
	Setting setting = settingRepository.findByUser(user);
	if (setting==null)
	  setting = createNewSetting(profile, user);
	return setting;
  }

  private Setting createNewSetting(Profile profile, User user) {
	Setting setting = new Setting(profile.getSelectedCorpus(), user);

	return settingRepository.save(setting);
  }
}
