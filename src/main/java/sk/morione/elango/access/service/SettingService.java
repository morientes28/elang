package sk.morione.elango.access.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.morione.elango.access.model.Setting;
import sk.morione.elango.access.repository.SettingRepository;

import javax.persistence.EntityNotFoundException;

/**
 * @author ⌐⨀_⨀¬ morientes on 16/11/2018.
 */
@Service
public class SettingService {

  private final Logger log = LoggerFactory.getLogger(SettingService.class);

  private SettingRepository repository;

  @Autowired
  public SettingService(SettingRepository repository) {

	this.repository = repository;
  }

  public Setting update(final Setting setting) {

	Setting dbS = repository.findByUser(setting.getUser());

	//TODO: I don't know why every save create a new record regardless of the same entity
	dbS.setCorpus(setting.getCorpus());
	dbS.setLanguage(setting.getLanguage());
	dbS.setRowsCount(setting.getRowsCount());
	dbS.setRowsCountReview(setting.getRowsCountReview());
	dbS.setSort(setting.getSort());
	dbS.setSortBy(setting.getSortBy());

	dbS = repository.save(dbS);
	log.info("Updated setting {}", dbS);

	return dbS;
  }


}
