package sk.morione.elango.access.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import sk.morione.elango.access.model.Role;
import sk.morione.elango.access.model.User;
import sk.morione.elango.access.repository.UserRepository;
import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.model.Language;
import sk.morione.elango.script.repository.CorpusRepository;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 26/10/2018.
 */
@Service
public class UserService {

  private final Logger log = LoggerFactory.getLogger(UserService.class);

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private CorpusRepository corpusRepository;

  @Autowired(required = false)
  private BCryptPasswordEncoder bCryptPasswordEncoder;

  public void save(User user) {

	// create user
	user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
	user.setRole(Role.USER);
	user = userRepository.save(user);

	// create corpus
	Corpus newCorpus = new Corpus(Language.EN, user.getUsername());
	newCorpus.setUser(user);
	newCorpus.setBase(false);
	newCorpus = corpusRepository.save(newCorpus);

	log.info("Create user {} with new corpus {}", user, newCorpus);
  }

  public User findByUsername(String username) {
	return userRepository.findByUsername(username);
  }
}