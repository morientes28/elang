package sk.morione.elango.core.config;

import org.apache.http.entity.ContentType;
import org.springframework.boot.actuate.endpoint.http.ActuatorMediaType;
import org.springframework.boot.actuate.endpoint.web.EndpointMediaTypes;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Arrays;
import java.util.List;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 11/10/2018.
 */
@Configuration
public class ActuatorEndpointConfig {

  private static final List<String> MEDIA_TYPES = Arrays
		  .asList(ContentType.APPLICATION_JSON.getMimeType(), ActuatorMediaType.V2_JSON);

  @Bean
  public EndpointMediaTypes endpointMediaTypes() {
	return new EndpointMediaTypes(MEDIA_TYPES, MEDIA_TYPES);
  }
}
