package sk.morione.elango.core.config;


import com.hazelcast.config.Config;
import com.hazelcast.config.EvictionPolicy;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MaxSizeConfig;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 06/11/2018.
 */
@Configuration
public class HazelcastConfig {

  @Bean
  public Config hazelCastConfig() {
	return new Config()
			.setInstanceName("hazelcast-instance")
			.addMapConfig(
					new MapConfig()
							.setName("scripts")
							.setMaxSizeConfig(new MaxSizeConfig(10000, MaxSizeConfig.MaxSizePolicy.FREE_HEAP_SIZE))
							.setEvictionPolicy(EvictionPolicy.LRU)
							.setTimeToLiveSeconds(300));
  }
}
