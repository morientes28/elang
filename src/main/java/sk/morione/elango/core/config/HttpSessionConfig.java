package sk.morione.elango.core.config;

import com.hazelcast.core.HazelcastInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import sk.morione.elango.core.metrics.UserMetrics;

import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.Map;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 06/11/2018.
 */
@Configuration
public class HttpSessionConfig {

  @Autowired(required = false)
  private HazelcastInstance hazelcastInstance;

  @Bean
  public HttpSessionListener httpSessionListener() {
	return new HttpSessionListener() {
	  @Override
	  public void sessionCreated(HttpSessionEvent hse) {

	  }

	  @Override
	  public void sessionDestroyed(HttpSessionEvent hse) {
		// add value to in memory grid - hazelcast
		Map<String, UserMetrics.User> ul = hazelcastInstance.getMap("users");
		ul.remove(hse.getSession().getId());
	  }
	};
  }
}
