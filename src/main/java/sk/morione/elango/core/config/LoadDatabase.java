package sk.morione.elango.core.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import sk.morione.elango.access.model.Role;
import sk.morione.elango.access.model.User;
import sk.morione.elango.access.repository.UserRepository;
import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.model.Evaluation;
import sk.morione.elango.script.model.Language;
import sk.morione.elango.script.model.Translation;
import sk.morione.elango.script.repository.CorpusRepository;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 02/10/2018.
 */
@Configuration
@Profile({"dev", "test"})
class LoadDatabase {

  private final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

  @Bean
  CommandLineRunner initDatabase(
		  CorpusRepository corpusRepository,
		  UserRepository userRepository) {
	return args -> {
	  log.info("Preloading " + createDefaultCorpusAndTranslations(
			  corpusRepository,
			  userRepository));
	};
  }

  private Corpus createDefaultCorpusAndTranslations(
		  CorpusRepository corpusRepository,
		  UserRepository userRepository) {

	Corpus corpusCheck = corpusRepository.findOneByLanguageAndBaseAndName(Language.EN, false, "my");
	if (corpusCheck != null)
	  return new Corpus();

	Role role = Role.USER;
	Role role2 = Role.ADMIN;

	User user = new User("user",
			"$2a$04$d3zhV4nEn0yq9ryUQLhfceuJBuzNXMf237rmFE7rexRtz41KznROy",
			null, role);
	user = userRepository.save(user);
	User user2 = new User("superuser",
			"$2a$04$d3zhV4nEn0yq9ryUQLhfceuJBuzNXMf237rmFE7rexRtz41KznROy",
			null, role2);
	user2 = userRepository.save(user2);

	Corpus corpus = new Corpus(Language.EN, "my");
	corpus.setUser(user);
	Translation translation = new Translation("I");
	translation.setEvaluation(Evaluation.KNOWN);
	translation.setTranscript("ja");
	translation.setTranscriptLanguage(Language.SK);
	translation.setCorpus(corpus);
	corpus.addTranslation(translation);
	corpus = corpusRepository.save(corpus);


	Translation translation2 = new Translation("you");
	translation2.setEvaluation(Evaluation.KNOWN);
	translation2.setTranscript("ty, vy");
	translation2.setTranscriptLanguage(Language.SK);
	translation2.setCorpus(corpus);
	corpus.addTranslation(translation2);
	corpus = corpusRepository.save(corpus);

	Translation translation3 = new Translation("is");
	translation3.setEvaluation(Evaluation.KNOWN);
	translation3.setTranscript("on, ona, ono");
	translation3.setTranscriptLanguage(Language.SK);
	translation3.setCorpus(corpus);
	corpus.addTranslation(translation3);
	corpus = corpusRepository.save(corpus);

	Translation translation4 = new Translation("have");
	translation4.setEvaluation(Evaluation.KNOWN);
	translation4.setTranscript("mať");
	translation4.setNote("v jednotnej osobe, alebo množnom čísle");
	translation4.setTranscriptLanguage(Language.SK);
	translation4.setCorpus(corpus);
	corpus = corpusRepository.save(corpus);

	Translation translation5 = new Translation("has");
	translation5.setEvaluation(Evaluation.KNOWN);
	translation5.setTranscript("má");
	translation5.setNote("v tretej osobe = ona má, on má, ono má");
	translation5.setTranscriptLanguage(Language.SK);
	translation5.setCorpus(corpus);
	corpus = corpusRepository.save(corpus);

	Corpus corpus2 = new Corpus(Language.EN, "my2");
	corpus2.setUser(user2);
	Translation translationN = new Translation("I");
	translationN.setEvaluation(Evaluation.KNOWN);
	translationN.setTranscript("ja");
	translationN.setTranscriptLanguage(Language.SK);
	translationN.setCorpus(corpus2);
	corpus2.addTranslation(translationN);
	corpusRepository.save(corpus2);

	return corpus;
  }
}
