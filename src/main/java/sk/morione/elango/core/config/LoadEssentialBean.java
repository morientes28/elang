package sk.morione.elango.core.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.model.Language;
import sk.morione.elango.script.repository.CorpusRepository;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 08/10/2018.
 */
@Component
public class LoadEssentialBean {

  public final static String BASE_CORPUS_ENG = "English base corpus";
  private final Logger log = LoggerFactory.getLogger(LoadEssentialBean.class);
  @Autowired
  private CorpusRepository corpusRepository;

  private Corpus cachedEnglishBaseCorpus;

  @Bean(name = "englishBaseCorpus")
  public Corpus englishBaseCorpus() {
	if (cachedEnglishBaseCorpus == null) {
	  log.info("Loading [{}]", BASE_CORPUS_ENG);
	  cachedEnglishBaseCorpus = corpusRepository.findOneByLanguageAndBaseAndName(
			  Language.EN, true, BASE_CORPUS_ENG);
	  if (cachedEnglishBaseCorpus == null) {
		log.warn(BASE_CORPUS_ENG + " has not found");
		cachedEnglishBaseCorpus = new Corpus(Language.EN, BASE_CORPUS_ENG);
		cachedEnglishBaseCorpus.setBase(true);
		cachedEnglishBaseCorpus = corpusRepository.save(cachedEnglishBaseCorpus);
	  }
	}

	return cachedEnglishBaseCorpus;

  }

}
