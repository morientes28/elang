package sk.morione.elango.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 * Configuration properties class for custom service attributes in application properties
 */
@Component
@EnableConfigurationProperties
@ConfigurationProperties(prefix = "service")
public class ServiceConfig {

  @NotNull
  private String tesseractDataPath;
  private String tmpPath;
  private String googleApiKey;
  private String appVersion;
  private String tinypngApikey;
  private String amazonEndpointUrl;
  private String amazonBucketName;
  private long schedulerInterval = 60000;
  private boolean enableTinyCommpresion;
  private String subtitlesApiUrl;

  public String getTesseractDataPath() {
	return tesseractDataPath;
  }

  public void setTesseractDataPath(String tesseractDataPath) {
	this.tesseractDataPath = tesseractDataPath;
  }

  public String getTmpPath() {
	return tmpPath;
  }

  public void setTmpPath(String tmpPath) {
	this.tmpPath = tmpPath;
  }

  public String getGoogleApiKey() {
	return googleApiKey;
  }

  public void setGoogleApiKey(String googleApiKey) {
	this.googleApiKey = googleApiKey;
  }

  public String getAppVersion() {
	return appVersion;
  }

  public void setAppVersion(String appVersion) {
	this.appVersion = appVersion;
  }

  public String getAmazonEndpointUrl() {
	return amazonEndpointUrl;
  }

  public void setAmazonEndpointUrl(String amazonEndpointUrl) {
	this.amazonEndpointUrl = amazonEndpointUrl;
  }

  public String getAmazonBucketName() {
	return amazonBucketName;
  }

  public void setAmazonBucketName(String amazonBucketName) {
	this.amazonBucketName = amazonBucketName;
  }

  public long getSchedulerInterval() {
	return schedulerInterval;
  }

  public void setSchedulerInterval(long schedulerInterval) {
	this.schedulerInterval = schedulerInterval;
  }

  public String getTinypngApikey() {
	return tinypngApikey;
  }

  public void setTinypngApikey(String tinypngApikey) {
	this.tinypngApikey = tinypngApikey;
  }

  public boolean isEnableTinyCommpresion() {
	return enableTinyCommpresion;
  }

  public void setEnableTinyCommpresion(boolean enableTinyCommpresion) {
	this.enableTinyCommpresion = enableTinyCommpresion;
  }

  public String getSubtitlesApiUrl() {
	return subtitlesApiUrl;
  }

  public void setSubtitlesApiUrl(String subtitlesApiUrl) {
	this.subtitlesApiUrl = subtitlesApiUrl;
  }
}
