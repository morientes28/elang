package sk.morione.elango.core.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import sk.morione.elango.access.model.Profile;
import sk.morione.elango.core.config.ServiceConfig;
import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.repository.CorpusRepository;
import sk.morione.elango.script.repository.ScriptRepository;
import sk.morione.elango.script.repository.TranslationRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.Map;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 08/10/2018.
 */
public class BaseController {

  public static final Map<String, String> JSON_RESULT_OK = Collections.singletonMap("response", "ok");

  @Autowired
  public TranslationRepository translationRepository;

  @Autowired
  public ScriptRepository scriptRepository;

  @Autowired
  public CorpusRepository corpusRepository;

  @Autowired
  public ServiceConfig config;

  @Autowired(required = false)
  private HttpSession session;

  @PersistenceContext
  private EntityManager em;

  @ModelAttribute("appVersion")
  public void appVersion(Model model) {
	model.addAttribute("appVersion", config.getAppVersion());
  }

  @ModelAttribute("points")
  public void points(Model model) {
	model.addAttribute("profile", activeProfile());
  }

  public Corpus activeCorpus() {
	return activeProfile().getSelectedCorpus();
  }

  public Profile activeProfile() {

	Profile profile = (Profile) session.getAttribute("profile");
	//TODO: better maintenance of detached entities
	if ((profile != null) && (!em.contains(profile.getSelectedCorpus()))) {
	  profile.setSelectedCorpus(corpusRepository.findOneById(profile.getSelectedCorpus().getId()));
	}
	return (profile == null) ? new Profile() : profile;
  }

}
