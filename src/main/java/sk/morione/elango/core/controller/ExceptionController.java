package sk.morione.elango.core.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.NoHandlerFoundException;
import sk.morione.elango.core.config.ServiceConfig;
import sk.morione.elango.script.controller.ScriptController;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 18/10/2018.
 */
@ControllerAdvice
public class ExceptionController {

  private final Logger log = LoggerFactory.getLogger(ScriptController.class);

  @Autowired
  public ServiceConfig config;

  @ModelAttribute("appVersion")
  public void appVersion(Model model) {
	model.addAttribute("appVersion", config.getAppVersion());
  }

  @ExceptionHandler(Exception.class)
  public String handleError(HttpServletRequest request, Exception e) {
	log.error("Request: " + request.getRequestURL() + " raised " + e);
	return "errors/500";
  }

  @ExceptionHandler(NoHandlerFoundException.class)
  public String handleError404(HttpServletRequest request, Exception e) {
	log.error("Request: " + request.getRequestURL() + " raised " + e);
	return "errors/404";
  }

  @ExceptionHandler(AccessDeniedException.class)
  public String handleError403(HttpServletRequest request, Exception e) {
	log.error("Request: " + request.getRequestURL() + " raised " + e);
	return "errors/403";
  }

  @ExceptionHandler(EntityNotFoundException.class)
  public String handleEntityNotFound(HttpServletRequest request, Exception e) {
	log.error("Request: " + request.getRequestURL() + " raised " + e);
	return "errors/notFound";
  }
}
