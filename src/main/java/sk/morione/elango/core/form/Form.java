package sk.morione.elango.core.form;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import sk.morione.elango.script.model.Language;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 23/10/2018.
 */
public abstract class Form {

  protected String baseUrl;
  protected int rows = 10;
  protected int page = 0;
  protected long maxPages = 0;
  protected long size = 0;
  protected String sortBy = "id";
  protected boolean desc;
  protected long userId;
  protected Language language = Language.EN;

  public Form(String baseUrl) {
	this.baseUrl = baseUrl;
  }

  public abstract String makeUrl(int page);

  public PageRequest getPageRequest() {

	Sort.Direction sorting = Sort.Direction.ASC;
	if (desc)
	  sorting = Sort.Direction.DESC;

	return PageRequest.of(page, rows, Sort.by(
			new Sort.Order(sorting, sortBy)));
  }

  public int getRows() {
	return rows;
  }

  public void setRows(int rows) {
	this.rows = rows;
  }

  public int getPage() {
	return page;
  }

  public void setPage(int page) {
	this.page = page;
  }

  public long getMaxPages() {
	return maxPages;
  }

  public void setMaxPages(long maxPages) {
	this.maxPages = maxPages;
  }

  public String getSortBy() {
	return sortBy;
  }

  public void setSortBy(String sortBy) {
	this.sortBy = sortBy;
  }

  public boolean isDesc() {
	return desc;
  }

  public void setDesc(boolean desc) {
	this.desc = desc;
  }

  public long getUserId() {
	return userId;
  }

  public void setUserId(long userId) {
	this.userId = userId;
  }

  public Language getLanguage() {
	return language;
  }

  public void setLanguage(Language language) {
	this.language = language;
  }

  public long getSize() {
	return size;
  }

  public void setSize(long size) {
	this.size = size;
  }
}
