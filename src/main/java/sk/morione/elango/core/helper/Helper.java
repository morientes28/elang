package sk.morione.elango.core.helper;

import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.morione.elango.access.controller.SettingController;

import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

/**
 * @author  ⌐⨀_⨀¬ morientes on 22/10/2018.
 */
public class Helper {

  private final Logger log = LoggerFactory.getLogger(SettingController.class);

  /**
   * returns a view (not a new list) of the sourceList for the
   * range based on page and pageSize
   *
   * @param sourceList
   * @param page,      page number should start from 1
   * @param pageSize
   * @return
   */
  public static <T> List<T> getPage(List<T> sourceList, int page, int pageSize) {
	if (pageSize <= 0 || page <= 0) {
	  throw new IllegalArgumentException("invalid page size: " + pageSize);
	}

	int fromIndex = (page - 1) * pageSize;
	if (sourceList == null || sourceList.size() < fromIndex) {
	  return Collections.emptyList();
	}

	// toIndex exclusive
	return sourceList.subList(fromIndex, Math.min(fromIndex + pageSize, sourceList.size()));
  }

  /**
   * Watching time in debugging or performance setting
   */
  public static Stopwatch watchTime() {
	return Stopwatch.createStarted();
  }

  public static void watchTime(Stopwatch stopwatch, String title) {
	if (stopwatch.isRunning()) {
	  stopwatch.stop();
	  System.out.println(String.format("WATCHTIME: Time elapsed for [%s] is %d ms",
			  title, stopwatch.elapsed(MILLISECONDS)));
	}
  }

  public static void TODO(String message){
	System.out.print("---------- " + message + " ----------");
	throw new UnsupportedOperationException("Not implemented yet");
  }

  public static String getIdFromYoutubeUrl(String url) {
	String pattern = "(?<=watch\\?v=|/videos/|embed\\/|youtu.be\\/|\\/v\\/|\\/e\\/|watch\\?v%3D|watch\\?feature=player_embedded&v=|%2Fvideos%2F|embed%\u200C\u200B2F|youtu.be%2F|%2Fv%2F)[^#\\&\\?\\n]*";

	Pattern compiledPattern = Pattern.compile(pattern);
	Matcher matcher = compiledPattern.matcher(url); //url is youtube url for which you want to extract the id.
	if (matcher.find()) {
	  return matcher.group();
	}
	return "";
  }
}
