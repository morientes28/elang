package sk.morione.elango.core.jobs;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import sk.morione.elango.core.config.ServiceConfig;
import sk.morione.elango.core.model.Scheduler;
import sk.morione.elango.core.repository.SchedulerRepository;
import sk.morione.elango.script.repository.ScriptRepository;
import sk.morione.elango.script.service.TranslationService;

import javax.transaction.Transactional;


/**
 * Created by  ⌐⨀_⨀¬ morientes on 15/10/2018.
 */
@Component
public class SchedulerTask {

  private static final Logger log = LoggerFactory.getLogger(SchedulerTask.class);
  private static final int COUNT_OF_PROCESSED_TASK = 10;

  @Autowired
  SchedulerRepository repository;

  @Autowired
  TranslationService service;

  @Autowired
  ScriptRepository scriptRepository;

  @Autowired
  ServiceConfig config;

  @Scheduled(fixedRateString = "${service.schedulerInterval}", initialDelay = 1000)
  @Transactional
  public void makeTranslation() {
	Pageable topTen = PageRequest.of(0, COUNT_OF_PROCESSED_TASK, Sort.by("id"));
	Page<Scheduler> schedules = repository.findAll(topTen);
	log.info("Scheduler task is starting with {} tasks", schedules.getTotalElements());
	schedules.forEach(s -> {
	  log.debug("Making translation corpus [{}], script [{}]", s.getCorpus().getId(), s.getScript().getId());
	  doTranslation(s);
	  s.getScript().setProcessed(true);
	  scriptRepository.save(s.getScript());
	  log.debug("Scheduler task [{}]is done", s.getId());
	  repository.delete(s);
	});
  }

  private void doTranslation(Scheduler scheduler) {
	scheduler.getCollectionWords()
			.forEach(w -> service.makeTranslation(
					w, scheduler.getCorpus(), scheduler.getScript()
			));
  }
}
