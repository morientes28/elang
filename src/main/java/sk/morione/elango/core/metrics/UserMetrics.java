package sk.morione.elango.core.metrics;

import com.hazelcast.core.HazelcastInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.endpoint.annotation.ReadOperation;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 06/11/2018.
 */
@Component
@Endpoint(id = "users")
public class UserMetrics {

  @Autowired(required = false)
  private HazelcastInstance hazelcastInstance;

  UserMetrics() {
  }

  @ReadOperation
  public List getAll() {

	return new ArrayList<>(hazelcastInstance.getMap("users").values());
  }

  public static class User implements Serializable {
	private String name;
	private String session;

	public User(String name) {
	  this.name = name;
	}

	public User(String name, String session) {
	  this.name = name;
	}

	public String getName() {
	  return this.name;
	}

	public void setName(String name) {
	  this.name = name;
	}


	public String getSession() {
	  return session;
	}

	public void setSession(String session) {
	  this.session = session;
	}
  }

}
