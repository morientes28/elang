package sk.morione.elango.core.model;

import sk.morione.elango.access.model.User;
import sk.morione.elango.script.model.Corpus;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 12/11/2018.
 */
@Entity
@SequenceGenerator(name = "seq", allocationSize = 100)
public class Activity extends Base implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
  private long id;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "user_id")
  private User user;

  private TypeActivity type;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "corpus_id")
  private Corpus corpus;

  private String flag;

  public Activity() {
  }

  public Activity(User user, TypeActivity type, String flag, Corpus corpus) {
	this.user = user;
	this.type = type;
	this.flag = flag;
	this.corpus = corpus;
  }

  public long getId() {
	return id;
  }

  public void setId(long id) {
	this.id = id;
  }

  public User getUser() {
	return user;
  }

  public void setUser(User user) {
	this.user = user;
  }

  public TypeActivity getType() {
	return type;
  }

  public void setType(TypeActivity type) {
	this.type = type;
  }

  public String getFlag() {
	return flag;
  }

  public void setFlag(String flag) {
	this.flag = flag;
  }

  public Corpus getCorpus() {
	return corpus;
  }

  public void setCorpus(Corpus corpus) {
	this.corpus = corpus;
  }

  @Override
  public boolean equals(Object o) {
	if (this == o) return true;
	if (!(o instanceof Activity)) return false;
	Activity activity = (Activity) o;
	return id == activity.id &&
			Objects.equals(user, activity.user) &&
			type == activity.type &&
			Objects.equals(flag, activity.flag);
  }

  @Override
  public int hashCode() {

	return Objects.hash(id, user, type, flag);
  }

  @Override
  public String toString() {
	return "Activity{" +
			"id=" + id +
			", user=" + user +
			", type=" + type +
			", flag='" + flag + '\'' +
			'}';
  }
}
