package sk.morione.elango.core.model;

import java.io.Serializable;


/**
 * @author ⌐⨀_⨀¬ morientes on 16/11/2018.
 */
public class ErrorResponse extends RestResponse implements Serializable {

  private ErrorResponse(String result, Error data) {
	super(result, data);
  }

  public static RestResponse create(Error data) {
	return new ErrorResponse("error", data);
  }

  class Error implements Serializable {

	public String code;
	public String message;

	public Error(String code, String message) {

	  this.code = code;
	  this.message = message;

	}
  }
}
