package sk.morione.elango.core.model;


import java.io.Serializable;

/**
 * Base abstract class of common rest response
 *
 * @author ⌐⨀_⨀¬ morientes on 16/11/2018.
 */
public class RestResponse implements Serializable {

  public final String result;
  public final Object data;

  RestResponse(String result, Object data) {
	this.result = result;
	this.data = data;
  }

  public static RestResponse create(Object data) {
	return new RestResponse("ok", data);
  }

}
