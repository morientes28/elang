package sk.morione.elango.core.model;

import org.apache.tomcat.util.buf.StringUtils;
import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.model.Script;

import javax.persistence.*;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 15/10/2018.
 */
@Entity
@SequenceGenerator(name = "seq", allocationSize = 100)
public class Scheduler {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
  private long id;

  @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  @JoinColumn(name = "script_id")
  private Script script;

  @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  @JoinColumn(name = "corpus_id")
  private Corpus corpus;

  @Lob
  @Column(name = "words", length = 6000, columnDefinition = "CLOB")
  private String words;

  public Scheduler() {

  }

  public Scheduler(Script script, Corpus corpus, Set<String> words) {
	this.script = script;
	this.corpus = corpus;
	this.words = StringUtils.join(words, ',');
  }

  public long getId() {
	return id;
  }

  public void setId(long id) {
	this.id = id;
  }

  public Script getScript() {
	return script;
  }

  public void setScript(Script script) {
	this.script = script;
  }

  public Corpus getCorpus() {
	return corpus;
  }

  public void setCorpus(Corpus corpus) {
	this.corpus = corpus;
  }

  public String getWords() {
	return words;
  }

  public void setWords(String words) {
	this.words = words;
  }

  public Set<String> getCollectionWords() {
	return new HashSet<>(Arrays.asList(this.words.split(",")));
  }
}
