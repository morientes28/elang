package sk.morione.elango.core.model;

import sk.morione.elango.script.model.Evaluation;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 30/09/2018.
 */
public enum TypeActivity {

  READ_SCRIPT, IMPORT_SCRIPT, REVIEW, EVALUATED_NEW, EVALUATED_LEARNING, EVALUATED_KNOWN, NONE;

  public static TypeActivity valueOf(Long id) {

	switch (id.intValue()) {
	  case 0:
		return READ_SCRIPT;
	  case 1:
		return IMPORT_SCRIPT;
	  case 2:
		return REVIEW;
	  case 3:
		return EVALUATED_NEW;
	  case 4:
		return EVALUATED_LEARNING;
	  case 5:
		return EVALUATED_KNOWN;
	}

	return NONE;
  }

  public static TypeActivity getTypeActivityByEvaluation(Evaluation evaluation) {

	switch (evaluation) {
	  case NEW:
		return EVALUATED_NEW;
	  case LEARNING:
		return EVALUATED_LEARNING;
	  case KNOWN:
		return EVALUATED_KNOWN;
	}
	return NONE;
  }
}
