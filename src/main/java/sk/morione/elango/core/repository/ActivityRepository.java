package sk.morione.elango.core.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import sk.morione.elango.access.model.User;
import sk.morione.elango.core.model.Activity;
import sk.morione.elango.core.model.TypeActivity;
import sk.morione.elango.script.model.Corpus;

import java.time.LocalDateTime;
import java.util.Optional;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 15/10/2018.
 */
@Repository
public interface ActivityRepository extends CrudRepository<Activity, Long> {

  Optional<Activity> findByUserAndTypeAndFlagAndCorpus(User user, TypeActivity type, String flag, Corpus corpus);

  long countActivitiesByUserAndTypeAndCorpus(User user, TypeActivity type, Corpus corpus);

  long countActivitiesByUserAndTypeAndCorpusAndCreatedOnAfter(User user, TypeActivity type, Corpus corpus, LocalDateTime after);
}
