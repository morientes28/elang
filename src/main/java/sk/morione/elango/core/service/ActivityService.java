package sk.morione.elango.core.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.morione.elango.access.model.User;
import sk.morione.elango.access.repository.UserRepository;
import sk.morione.elango.core.model.Activity;
import sk.morione.elango.core.model.TypeActivity;
import sk.morione.elango.core.repository.ActivityRepository;
import sk.morione.elango.script.model.Corpus;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 12/11/2018.
 */
@Service
public class ActivityService {

  private final Logger log = LoggerFactory.getLogger(ActivityService.class);
  @Autowired
  UserRepository userRepository;
  @Autowired
  ActivityRepository repository;

  public void activity(String username, TypeActivity type, String flag, Corpus corpus) {

	User user = userRepository.findByUsername(username);

	if (!repository.findByUserAndTypeAndFlagAndCorpus(user, type, flag, corpus).isPresent()) {
	  repository.save(new Activity(user, type, flag, corpus));
	}
	log.debug("Activity {} for user {} with flag {}", type, user, flag);
  }
}
