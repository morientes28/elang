package sk.morione.elango.script.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import sk.morione.elango.core.config.ServiceConfig;
import sk.morione.elango.core.controller.BaseController;
import sk.morione.elango.core.helper.Helper;
import sk.morione.elango.core.model.TypeActivity;
import sk.morione.elango.core.service.ActivityService;
import sk.morione.elango.script.form.ImportForm;
import sk.morione.elango.script.model.Level;
import sk.morione.elango.script.service.MatcherService;
import sk.morione.elango.script.service.TranslationService;
import sk.morione.elango.storage.service.FileProcessService;

import java.time.Instant;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@Controller
public class ImportController extends BaseController {

  private final Logger log = LoggerFactory.getLogger(ImportController.class);

  @Autowired
  FileProcessService fileProcessService;

  @Autowired
  TranslationService translationService;

  @Autowired
  MatcherService matcherService;

  @Autowired
  ActivityService activityService;

  @Autowired
  ServiceConfig config;


  @GetMapping("/import")
  public String importScript(Model model) {

	setModel(model, new ImportForm());

	return "import";
  }

  @GetMapping("/bookmark")
  public String bookmark(final ImportForm importForm, Model model) {

	String idVideo = Helper.getIdFromYoutubeUrl(importForm.getEmbedUrl());
	importForm.setEditordata(getSubtatlesFromRest(idVideo));
	importForm.setImageUrl(getYoutubeImage(idVideo));

	setModel(model, importForm);

	return "bookmark";
  }

  @PostMapping("/import")
  public String handleFileUpload(final ImportForm importForm,
								 RedirectAttributes redirectAttributes) {

	long id = handleImport(importForm, false);

	log.debug("Import data from file");

	redirectAttributes.addFlashAttribute("message",
			"You have been successfully uploaded source to your script list. " +
					"Right now your source is processing. It will take several seconds to finish.");

	return "redirect:/scripts/" + id;
  }

  @PostMapping("/import/superuser")
  public String importSuperuser(final ImportForm importForm,
								RedirectAttributes redirectAttributes) {

	long id = handleImport(importForm, true);

	log.debug("Import data from file");

	redirectAttributes.addFlashAttribute("message",
			"You have been successfully uploaded source to your script list. " +
					"Right now your source is processing. It will take several seconds to finish.");

	return "redirect:/scripts/" + id;
  }

  private String getYoutubeImage(String video_id) {
	return "http://img.youtube.com/vi/" + video_id + "/0.jpg";
  }

  private String getSubtatlesFromRest(String video_id) {
	return new RestTemplate().getForObject(config.getSubtitlesApiUrl() + video_id, String.class);
  }


  private long handleImport(ImportForm importForm, boolean isSuperUser) {

	String urlImage = importForm.getImageUrl();
	String ocrText = importForm.getEditordata();

	if (!isSuperUser)
	  importForm.setUserId(activeProfile().getUserId());

	if (importForm.getImage() != null
			&& !importForm.getImage().isEmpty()
			&& importForm.getImageUrl() == null) {

	  urlImage = fileProcessService.processFile(importForm.getImage());
	}

	if (importForm.getFile() != null) {
	  ocrText = fileProcessService.extractFromFileOrText(
			  importForm.getFile(), importForm.getEditordata()
	  );
	}

	activityService.activity(
			activeProfile().getUsername(),
			TypeActivity.IMPORT_SCRIPT,
			String.valueOf(Instant.now().getEpochSecond()),
			activeProfile().getSelectedCorpus());

	return translationService.saveScript(ocrText, activeCorpus(), importForm, urlImage);

  }

  private void setModel(Model model, ImportForm importForm) {
	model.addAttribute("levels", Level.getAll());
	model.addAttribute("importForm", importForm);
	model.addAttribute("importUrl",
			activeProfile().isAdmin() ? "/import/superuser" : "/import");
  }


}
