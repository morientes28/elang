package sk.morione.elango.script.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import sk.morione.elango.script.model.Lesson;
import sk.morione.elango.script.model.Script;
import sk.morione.elango.script.service.TranslationService;

import java.util.List;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@RestController
public class LessonRestController {

  @Autowired
  TranslationService service;

  // Aggregate root

  @GetMapping("/lesson")
  List<Lesson> all() {
	return service.getAllLessonSortByCreatedOn();
  }

  @PostMapping("/lesson")
  List<Lesson> newLesson() {
	return service.getAllLessonSortByCreatedOn();
  }

  // Single item

  @RequestMapping("/lesson/{idL}/{id}")
  public Script lesson(@PathVariable("idL") long idL, @PathVariable("id") int id) {
	return service.getScriptByLessonAndPage(idL, id);
  }
}
