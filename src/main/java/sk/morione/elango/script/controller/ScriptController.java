package sk.morione.elango.script.controller;

import net.rossillo.spring.web.mvc.CacheControl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import sk.morione.elango.core.controller.BaseController;
import sk.morione.elango.core.helper.Helper;
import sk.morione.elango.core.model.TypeActivity;
import sk.morione.elango.core.service.ActivityService;
import sk.morione.elango.script.form.SearchScriptForm;
import sk.morione.elango.script.model.Evaluation;
import sk.morione.elango.script.model.Level;
import sk.morione.elango.script.model.Script;
import sk.morione.elango.script.model.Translation;
import sk.morione.elango.script.service.MatcherService;
import sk.morione.elango.script.service.ScriptService;
import sk.morione.elango.script.service.TranslationService;
import sk.morione.elango.storage.service.FileProcessService;

import java.util.ArrayList;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@Controller
public class ScriptController extends BaseController {

  private final Logger log = LoggerFactory.getLogger(ScriptController.class);
  @Autowired
  FileProcessService fileProcessService;

  @Autowired
  ScriptService scriptService;

  @Autowired
  TranslationService translationService;

  @Autowired
  MatcherService matcherService;

  @Autowired
  ActivityService activityService;

  @CacheControl(maxAge = 300)
  @GetMapping({"/", "/scripts"})
  public String all(final SearchScriptForm searchForm, Model model) {

	//TODO: set profile to searchform
	searchForm.setUserId(activeProfile().getUserId());
	searchForm.setLanguage(activeCorpus().getLanguage());

	//TODO: to service or private method - set properties by user settings
	searchForm.setRows(activeProfile().getSetting().getRowsCount());
	searchForm.setDesc(activeProfile().getSetting().getSort().equals("desc"));
	searchForm.setSortBy(activeProfile().getSetting().getSortBy());

	Page<Script> scripts = scriptService.getByCritery(
			searchForm,
			searchForm.getPageRequest()
	);
	searchForm.setMaxPages(scripts.getTotalPages());
	searchForm.setSize(scripts.getTotalElements());
	setModel(model, scripts, searchForm);

	return "scripts";
  }


  private void setModel(Model model, Object... attributes) {
	model.addAttribute("scripts", attributes[0]);
	model.addAttribute("searchForm", attributes[1]);
  }

  @CacheControl(maxAge = 300)
  @GetMapping("/scripts/{id}")
  public String all(
		  @PathVariable("id") final Long id, Model model) {

	Script script = translationService.getScriptById(id);
	translationService.addScriptToUser(script, activeProfile());
	Set<Translation> corpusTrans = translationService.getAllUnknownTransactionsByCorpus(activeCorpus());
	activityService.activity(
			activeProfile().getUsername(),
			TypeActivity.READ_SCRIPT,
			String.valueOf(script.getId()),
			activeProfile().getSelectedCorpus());

	model.addAttribute("script", script);
	model.addAttribute("corpusTranslations", corpusTrans);
	model.addAttribute("newTranslation", new Translation("", script));

	int translation_count = activeProfile().getSetting().getRowsCount();
	model.addAttribute("translations",
			Helper.getPage(
					new ArrayList<>(script.getTranslations()
							.stream()
							.filter(s -> !s.getEvaluation().equals(Evaluation.KNOWN))
							.filter(s -> !s.getEvaluation().equals(Evaluation.IGNORE))
							.collect(Collectors.toList())),
					1,
					translation_count)
	);
	model.addAttribute("page", 1);
	//TODO: do testing
	int n = script.getTranslations().size() / translation_count;
	if ((script.getTranslations().size() % translation_count != 0))
	  n += 1;
	model.addAttribute("pageMax", n);
	log.debug("Loaded detail script [{}]", id);

	return "script";

  }

  @GetMapping("/scripts/{id}/translations/{page}")
  public String translationsByPage(
		  @PathVariable("id") final Long id,
		  @PathVariable("page") Integer page,
		  Model model) {

	Script script = translationService.getScriptById(id);
	Set<Translation> corpusTrans = translationService.getAllUnknownTransactionsByCorpus(activeCorpus());
	model.addAttribute("script", script);

	int translation_count = activeProfile().getSetting().getRowsCount();
	model.addAttribute("translations",
			Helper.getPage(
					new ArrayList<>(script.getTranslations()),
					page,
					translation_count)
	);

	model.addAttribute("page", page);
	model.addAttribute("pageMax", script.getTranslations().size() / translation_count);
	log.debug("Loaded detail script [{}]", id);

	return "translation :: translations";

  }

  @GetMapping("/scripts/{id}/edit")
  public String editScript(@PathVariable final long id, Model model) {

	Script script = translationService.getScriptById(id);
	model.addAttribute("script", script);
	model.addAttribute("levels", Level.getAll());
	log.debug("Edit script [{}]", id);

	return "scriptEdit";
  }

  @GetMapping("/scripts/delete/{id}")
  public String deleteScript(@PathVariable final long id) {

	translationService.deleteScriptById(id);
	log.debug("Deleted script [{}]", id);

	return "redirect:/scripts";
  }

  @GetMapping("/scripts/unread/{id}")
  public String removeScript(@PathVariable final long id) {

	translationService.unreadScriptByIdAndProfile(id, activeProfile());
	log.debug("Unread script [{}]", id);

	return "redirect:/scripts";
  }

}
