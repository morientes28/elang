package sk.morione.elango.script.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import sk.morione.elango.core.controller.BaseController;
import sk.morione.elango.script.service.StatisticsService;

import javax.transaction.Transactional;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@Controller
public class StatisticsController extends BaseController {

  private final Logger log = LoggerFactory.getLogger(StatisticsController.class);


  @Autowired
  StatisticsService service;


  @GetMapping("/statistics")
  @Transactional
  public String all(Model model) {

	model.addAttribute("statistics", service.getStatistics(activeProfile()));
	return "statistics";

  }

}
