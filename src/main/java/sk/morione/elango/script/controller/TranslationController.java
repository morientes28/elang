package sk.morione.elango.script.controller;

import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import sk.morione.elango.core.controller.BaseController;
import sk.morione.elango.script.form.SearchForm;
import sk.morione.elango.script.model.Script;
import sk.morione.elango.script.model.Translation;
import sk.morione.elango.script.service.TranslationService;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@Controller
public class TranslationController extends BaseController {

  private final Logger log = LoggerFactory.getLogger(TranslationController.class);


  @Autowired
  TranslationService service;

  @ModelAttribute
  public List<Script> scriptList() {
	return Lists.newArrayList(scriptRepository.findAll());
  }

  @GetMapping("/translation")
  @Transactional
  public String all(final SearchForm searchForm, Model model) {

	//TODO:search test
	//TODO: private method
	//TODO: to service or private method - set properties by user settings
	searchForm.setRows(activeProfile().getSetting().getRowsCount());
	searchForm.setDesc(activeProfile().getSetting().getSort().equals("desc"));
	searchForm.setSortBy(activeProfile().getSetting().getSortBy());

	Page<Translation> translations;
	if (searchForm.getEvaluations().isEmpty()) {
	  translations = translationRepository.findAllBySearchForm(
			  activeCorpus(),
			  searchForm.getScript(),
			  searchForm.getPageRequest());
	} else {
	  translations = translationRepository.findAllBySearchForm(
			  activeCorpus(),
			  searchForm.getScript(),
			  searchForm.getEnumEvaluations(),
			  searchForm.getPageRequest());
	}
	searchForm.setMaxPages(translations.getTotalPages());
	searchForm.setSize(translations.getTotalElements());

	String view = "translation";

	if (searchForm.isReview()) {
	  view = "review";
	  if (translations.getTotalElements() > 0) {

		int count = activeProfile().getSetting().getRowsCountReview();

		if (translations.getTotalElements() < count)
		  count = Long.valueOf(translations.getTotalElements()).intValue();
		translations = translationRepository.findAllBySearchForm(
				activeCorpus(),
				searchForm.getScript(),
				searchForm.getEnumEvaluations(),
				PageRequest.of(
						0, Long.valueOf(translations.getTotalElements()).intValue(),
						Sort.by(new Sort.Order(Sort.Direction.DESC, "updatedOn")))
		);

		translations = service.randomTransation(count, translations);
	  }
	}

	searchForm.setReview(false);
	model.addAttribute("translations", translations);
	model.addAttribute("searchForm", searchForm);
	log.debug("Loaded translations of size: " + translations.getTotalElements());

	return view;

  }

}
