package sk.morione.elango.script.form;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 18/10/2018.
 */
public class ImportForm {

  private MultipartFile file;
  private String editordata;
  private String embedUrl;
  private MultipartFile image;
  private String imageUrl;
  private String title;
  private Long userId;
  private int level;

  public ImportForm() {
  }

  public ImportForm(long userId) {
	this.userId = userId;
  }

  public MultipartFile getFile() {
	return file;
  }

  public void setFile(MultipartFile file) {
	this.file = file;
  }

  public String getEditordata() {
	return editordata;
  }

  public void setEditordata(String editordata) {
	this.editordata = editordata;
  }

  public String getEmbedUrl() {
	return embedUrl;
  }

  public void setEmbedUrl(String embedUrl) {
	this.embedUrl = embedUrl;
  }

  public MultipartFile getImage() {
	return image;
  }

  public void setImage(MultipartFile image) {
	this.image = image;
  }

  public String getTitle() {
	return title;
  }

  public void setTitle(String title) {
	this.title = title;
  }

  public int getLevel() {
	return level;
  }

  public void setLevel(int level) {
	this.level = level;
  }

  public Long getUserId() {
	return userId;
  }

  public void setUserId(Long userId) {
	this.userId = userId;
  }

  public String getImageUrl() {
	return imageUrl;
  }

  public void setImageUrl(String imageUrl) {
	this.imageUrl = imageUrl;
  }
}
