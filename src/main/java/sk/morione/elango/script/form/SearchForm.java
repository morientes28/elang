package sk.morione.elango.script.form;

import sk.morione.elango.core.form.Form;
import sk.morione.elango.script.model.Evaluation;

import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 09/10/2018.
 */
public class SearchForm extends Form implements Serializable {

  private String script = "%";
  private List<Long> evaluations = Arrays.asList(0L, 1L, 2L);
  private boolean review;
  private long scriptId;

  public SearchForm() {
	super("/translation");
  }

  public SearchForm(String baseUrl) {
	super(baseUrl);
  }

  public String getScript() {
	return script;
  }

  public void setScript(String script) {
	this.script = script;
  }

  public List<Long> getEvaluations() {
	return evaluations;
  }

  public void setEvaluations(List<Long> evaluations) {
	this.evaluations = evaluations;
  }

  public boolean isReview() {
	return review;
  }

  public void setReview(boolean review) {
	this.review = review;
  }

  public long getScriptId() {
	return scriptId;
  }

  public void setScriptId(long scriptId) {
	this.scriptId = scriptId;
  }

  public List<Evaluation> getEnumEvaluations() {

	return evaluations.stream()
			.map(Evaluation::valueOf)
			.collect(Collectors.toList());
  }

  @Override
  public String makeUrl(int page) {
	StringBuilder eval = new StringBuilder();
	for (Long e : evaluations) {
	  eval.append("&evaluations=").append(e);
	}
	return baseUrl + "?script=" + script
			+ "&review=" + review
			+ "&rows=" + rows
			+ "&sortBy=" + sortBy
			+ "&desc=" + desc
			+ eval
			+ "&page=" + page;
  }

  @Override
  public String toString() {
	return "SearchForm{" +
			"script='" + script + '\'' +
			'}';
  }
}
