package sk.morione.elango.script.form;

import sk.morione.elango.core.form.Form;

import java.io.Serializable;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 17/10/2018.
 */
public class SearchScriptForm extends Form implements Serializable {

  private String text = "";
  private boolean publ = true;
  private boolean priv = true;
  private boolean read = true;

  public SearchScriptForm() {
	super("/scripts");
	desc = true;
  }

  public SearchScriptForm(String baseUrl) {

	super(baseUrl);
	desc = true;
  }

  public String getText() {
	return text;
  }

  public void setText(String text) {
	this.text = text;
  }

  public boolean isPubl() {
	return publ;
  }

  public void setPubl(boolean publ) {
	this.publ = publ;
  }

  public boolean isPriv() {
	return priv;
  }

  public void setPriv(boolean priv) {
	this.priv = priv;
  }

  public boolean isRead() {
	return read;
  }

  public void setRead(boolean read) {
	this.read = read;
  }

  @Override
  public String makeUrl(int page) {

	return baseUrl + "?text=" + text
			+ "&priv=" + priv
			+ "&publ=" + publ
			+ "&read=" + read
			+ "&rows=" + rows
			+ "&sortBy=" + sortBy
			+ "&desc=" + desc
			+ "&page=" + page;
  }

  @Override
  public String toString() {
	return "SearchScriptForm{" +
			"text='" + text + '\'' +
			", rows=" + rows +
			", priv=" + priv +
			", read=" + read +
			", publ=" + publ +
			", page=" + page +
			", sortBy='" + sortBy + '\'' +
			", desc=" + desc +
			'}';
  }
}
