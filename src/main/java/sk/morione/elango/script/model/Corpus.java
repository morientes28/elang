package sk.morione.elango.script.model;

import sk.morione.elango.access.model.User;
import sk.morione.elango.core.model.Base;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 30/09/2018.
 */

@Entity
@SequenceGenerator(name = "seq", allocationSize = 100)
public class Corpus extends Base implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
  private long id;

  private Language language;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User user;

  private String name;

  private boolean base = false;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "corpus", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  private Set<Translation> translations = new HashSet<>();

  public Corpus() {
  }

  public Corpus(Language language, String name) {

	this.language = language;
	this.name = name;
  }

  public Corpus(Language language, String name, User user) {
	this.language = language;
	this.name = name;
	this.user = user;
  }

  public long getId() {
	return id;
  }

  public void setId(long id) {
	this.id = id;
  }

  public Language getLanguage() {
	return language;
  }

  public void setLanguage(Language language) {
	this.language = language;
  }

  public Set<Translation> getTranslations() {
	return translations;
  }

  public void setTranslations(Set<Translation> translations) {
	this.translations = translations;
  }

  public boolean addTranslation(Translation translation) {

	if (!translations.contains(translation)) {
	  this.translations.add(translation);
	  return true;
	}

	return false;
  }

  public void addTranslations(List<Translation> translations) {
	this.translations.addAll(translations);
  }

  public String getName() {
	return name;
  }

  public void setName(String name) {
	this.name = name;
  }

  public boolean isBase() {
	return base;
  }

  public void setBase(boolean base) {
	this.base = base;
  }

  public void removeTranslation(Translation translation) {
	translations.remove(translation);
  }


  public User getUser() {
	return user;
  }

  public void setUser(User user) {
	this.user = user;
  }

  @Override
  public boolean equals(Object o) {
	if (this == o) return true;
	if (!(o instanceof Corpus)) return false;
	Corpus corpus = (Corpus) o;
	return id == corpus.id;
  }

  @Override
  public int hashCode() {

	return Objects.hash(id, language);
  }

  @Override
  public String toString() {
	return "Corpus{" +
			"id=" + id +
			", language=" + language +
			", name='" + name + '\'' +
			", base=" + base +
			'}';
  }
}
