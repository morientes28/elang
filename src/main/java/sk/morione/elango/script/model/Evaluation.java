package sk.morione.elango.script.model;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 30/09/2018.
 */
public enum Evaluation {

  NEW, LEARNING, KNOWN, IGNORE;

  public static Evaluation valueOf(Long id) {
	switch (id.intValue()) {
	  case 0:
		return NEW;
	  case 1:
		return LEARNING;
	  case 2:
		return KNOWN;
	  default:
		return IGNORE;

	}
  }
}
