package sk.morione.elango.script.model;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 30/09/2018.
 */
public enum Language {

  EN(0, "EN"), SK(1, "SK"), ES(2, "ES"), IT(3, "IT"), DE(4, "DE");

  private long id;
  private String name;

  Language(long id, String name) {
	this.id = id;
	this.name = name;
  }

  public static Language valueOf(long id) {
	switch ((int) id) {
	  case 0:
		return EN;
	  case 1:
		return SK;
	  case 2:
		return ES;
	  case 3:
		return IT;
	  case 4:
		return DE;
	  default:
		return EN;
	}
  }

  public long getId() {
	return id;
  }

  public String getName() {
	return name;
  }
}
