package sk.morione.elango.script.model;

import org.springframework.lang.NonNull;
import sk.morione.elango.core.model.Base;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 30/09/2018.
 */

@Entity
@SequenceGenerator(name = "seq", allocationSize = 100)
public class Lesson extends Base implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
  private long id;


  @OneToMany(mappedBy = "lesson", cascade = CascadeType.ALL)
  private List<Script> scripts = new ArrayList<>();

  @NonNull
  private String name;

  public Lesson() {

  }

  public long getId() {
	return id;
  }

  public void setId(long id) {
	this.id = id;
  }

  @NonNull
  public String getName() {
	return name;
  }

  public void setName(@NonNull String name) {
	this.name = name;
  }

  public List<Script> getScripts() {
	return scripts;
  }

  public void setScripts(List<Script> scripts) {
	this.scripts = scripts;
  }

  public void addScript(Script script) {
	this.scripts.add(script);
  }

  @Override
  public boolean equals(Object o) {
	if (this == o) return true;
	if (!(o instanceof Lesson)) return false;
	Lesson lesson = (Lesson) o;
	return id == lesson.id &&
			Objects.equals(scripts, lesson.scripts) &&
			Objects.equals(name, lesson.name);
  }

  @Override
  public int hashCode() {

	return Objects.hash(id, scripts, name);
  }
}
