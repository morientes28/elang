package sk.morione.elango.script.model;

import java.util.Arrays;
import java.util.List;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 30/09/2018.
 */
public enum Level {

  BEGINNER(0, "beginner"),
  ELEMENTARY(1, "elementary"),
  INTERMEDIATE(2, "intermediate"),
  UPPER_INTERMEDIATE(3, "upper-intermediate"),
  ADVANCE(4, "advance"),
  PROFICIENCY(5, "proficiency");

  private String name;
  private int id;

  Level(int id, String name) {
	this.id = id;
	this.name = name;
  }

  public static Level valueOf(Long id) {
	switch (id.intValue()) {
	  case 0:
		return BEGINNER;
	  case 1:
		return ELEMENTARY;
	  case 2:
		return INTERMEDIATE;
	  case 3:
		return UPPER_INTERMEDIATE;
	  case 4:
		return ADVANCE;
	  case 5:
		return PROFICIENCY;
	  default:
		return BEGINNER;

	}
  }

  public static List<Level> getAll() {
	return Arrays.asList(
			BEGINNER, ELEMENTARY, INTERMEDIATE, UPPER_INTERMEDIATE, ADVANCE, PROFICIENCY
	);
  }

  public int getId() {
	return id;
  }

  public String getName() {
	return name;
  }

}
