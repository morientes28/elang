package sk.morione.elango.script.model;

import com.google.common.base.Strings;
import org.springframework.util.StringUtils;
import sk.morione.elango.access.model.User;
import sk.morione.elango.core.model.Base;

import javax.persistence.*;
import java.io.File;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@Entity
@SequenceGenerator(name = "seq", allocationSize = 100)
public class Script extends Base implements Serializable {

  private final static String DEFAULT_MEDIA_URL = "default.jpg";
  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
  private long id;

  private Language language = Language.EN;

  @Lob
  @Column(name = "content", length = 6000, columnDefinition = "CLOB")
  private String content;

  private String media; // url images
  private String embedUrl;

  private String title;
  private String author;
  private String source;
  private int page = 1;
  private boolean processed = false;
  private Level level;

  @Transient
  private int countNewTranslations = 0;

  @Transient
  private int countAllTranslations = 0;

  //@Transient
  //private int countTranslationsInProgress = 0;

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "lesson_id", nullable = true)
  private Lesson lesson;

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "script", cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  @OrderBy("id asc")
  private Set<Translation> translations = new HashSet<>();

  @ManyToMany(fetch = FetchType.LAZY,
		  cascade = {
				  CascadeType.PERSIST,
				  CascadeType.MERGE
		  })
  @JoinTable(name = "script_users",
		  joinColumns = {@JoinColumn(name = "script_id")},
		  inverseJoinColumns = {@JoinColumn(name = "user_id")})
  private Set<User> users = new HashSet<>();

  @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
  @JoinColumn(name = "owner_id", nullable = true)
  private User owner;

  public Script() {
  }

  public Script(String content) {
	this.content = content;
  }

  public Script(String content, String title, String media) {
	this.content = content;
	this.title = title;
	this.media = media;
  }

  public Set<String> getWordsFromContent() {

	contentConsolidation();

	String[] split = content
			.replaceAll("&.*?;", " ")
			.replaceAll("<[^>]*>", " ")
			.replaceAll("[^\\p{IsAlphabetic}^\\p{Space}^\\-’']", "")
			.toLowerCase()
			.split(" ");
	Set<String> result = new HashSet<>(Arrays.asList(split));
	countAllTranslations = split.length;

	// result empty string consolidation
	result.remove("");
	result.remove("'");
	result.remove("’");
	result.remove(" ");
	return result;
  }

  @PrePersist
  public void prePersist() {
	if (Strings.isNullOrEmpty(title)) {
	  DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy_HHmmss");
	  title = "unknown_" + LocalDateTime.now().format(formatter);
	}
  }

  public long getId() {
	return id;
  }

  public void setId(long id) {
	this.id = id;
  }

  public Language getLanguage() {
	return language;
  }

  public void setLanguage(Language language) {
	this.language = language;
  }

  public String getContent() {
	return content;
  }

  public void setContent(String content) {
	this.content = content;
  }

  public String getMedia() {
	if (StringUtils.isEmpty(media)) {
	  return DEFAULT_MEDIA_URL;
	}
	if (!media.contains("http") && !media.contains("s3")) {
	  media = "/" + new File(media).getName();
	}
	return media;
  }

  public void setMedia(String media) {
	this.media = media;
  }

  public String getAuthor() {
	return author;
  }

  public void setAuthor(String author) {
	this.author = author;
  }

  public String getSource() {
	return source;
  }

  public void setSource(String source) {
	this.source = source;
  }

  public String getTitle() {
	return title;
  }

  public void setTitle(String title) {
	this.title = title;
  }

  public int getPage() {
	return page;
  }

  public void setPage(int page) {
	this.page = page;
  }

  public Lesson getLesson() {
	return lesson;
  }

  public void setLesson(Lesson lesson) {
	this.lesson = lesson;
  }

  public Set<Translation> getTranslations() {
	return translations;
  }

  public void setTranslations(Set<Translation> translations) {
	this.translations = translations;
  }

  public Level getLevel() {
	return level;
  }

  public void setLevel(Level level) {
	this.level = level;
  }

  public boolean isProcessed() {
	return processed;
  }

  public void setProcessed(boolean processed) {
	this.processed = processed;
  }

  public User getOwner() {
	return owner;
  }

  public void setOwner(User owner) {
	this.owner = owner;
  }

  public String getEmbedUrl() {
	//accept :  https://www.youtube.com/embed/wmuj8w6s5f8
	//			http://youtu.be/wmuj8w6s5f8

	String id;
	if (StringUtils.hasText(embedUrl)) {
	  if (!embedUrl.contains("embed")) {
		if (embedUrl.contains("v="))
		  id = embedUrl.substring(embedUrl.lastIndexOf("v=") + 2);
		else
		  id = embedUrl.substring(embedUrl.lastIndexOf("/") + 1);
		embedUrl = "https://www.youtube.com/embed/" + id;
	  }
	}
	return embedUrl;
  }

  public void setEmbedUrl(String embedUrl) {
	this.embedUrl = embedUrl;
  }

  /**
   * Get count of saved new translations only in Script
   *
   * @return
   */
  public long getCountNewTranslations() {
	return translations.stream()
			.filter(t -> (t.getEvaluation().equals(Evaluation.NEW)
					|| t.getEvaluation().equals(Evaluation.LEARNING))
			).count();

  }

  /**
   * Get count of unknown translations per Script from all corpus
   * @return
   */
  //public int getCountAllTranslationsInProgress(){
  //  return countTranslationsInProgress;
  //}

  /**
   * Get all count of translations in Script
   *
   * @return
   */
  public int getAllCountTranslations() {
	if (countAllTranslations == 0)
	  countAllTranslations = getWordsFromContent().size();
	return countAllTranslations;
  }

  /**
   * Market content only from translations which were saved during importing Script
   *
   * @return HTML marked text
   */
  public String getMarkedContent() {

	contentConsolidation();
	translations.forEach(this::markering);
	return content;
  }

  /**
   * Market content includes all unknown translations from user corpus
   *
   * @param translations
   * @return
   */
  public String getMarkedContent(Set<Translation> translations) {

	contentConsolidation();
	translations.forEach(this::markering);
	//countTranslationsInProgress = StringUtils.countOccurrencesOf(content, "eval ");
	return content;
  }

  public Set<User> getUsers() {
	return users;
  }

  public void setUsers(Set<User> users) {
	this.users = users;
  }

  public void addUser(User user) {
	this.users.add(user);
  }

  public void removeUser(User user) {
	this.users.remove(user);
  }


  private void contentConsolidation() {
	// content consolidation
	if (!content.startsWith(" "))
	  content = " " + content;
	if (!content.endsWith(" "))
	  content = content + " ";

	content = content.replace(".", " . ");
	content = content.replace("(", " ( ");
	content = content.replace(")", " ) ");
	content = content.replace(",", " , ");
	content = content.replace(":", " : ");
	content = content.replace("<br>", "");
  }

  private void markering(Translation translation) {

	if (translation.getOrigin().equals(" ") || translation.getOrigin().equals(""))
	  return;

	String flag = "";
	if (translation.getScript() != null)
	  if (translation.getScript().getId() == this.getId())
		flag = " eval-actual";
	switch (translation.getEvaluation()) {

	  case NEW:
		content = content.replace(
				' ' + translation.getOrigin() + ' ',
				" <span class='eval eval-new" + flag + "'>" + translation.getOrigin() + "</span> ");
		break;
	  case LEARNING:
		content = content.replace(
				' ' + translation.getOrigin() + ' ',
				" <span class='eval eval-learning" + flag + "'>" + translation.getOrigin() + "</span> ");
		break;
	}
  }

  @Override
  public boolean equals(Object o) {
	if (this == o) return true;
	if (!(o instanceof Script)) return false;
	Script script = (Script) o;
	return id == script.id &&
			page == script.page &&
			language == script.language;
  }

  @Override
  public int hashCode() {

	return Objects.hash(id, language, content);
  }

  @Override
  public String toString() {
	return "Script{" +
			"id=" + id +
			'}';
  }
}
