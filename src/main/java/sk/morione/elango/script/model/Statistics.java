package sk.morione.elango.script.model;

import java.io.Serializable;
import java.util.Arrays;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 12/11/2018.
 */
public class Statistics implements Serializable {

  // index
  public static final int NEW_WORDS = 0;
  public static final int LEARNING_WORDS = 1;
  public static final int KNOWN_WORDS = 2;
  public static final int COUNT_OF_IMPORTED_SCRIPTS = 3;
  public static final int COUNT_OF_READ_SCRIPTS = 4;
  public static final int SIZE = 6;

  private long[] all;
  private long[] daily;
  private long[] weekly;

  private long sizeActiveVocabulary;
  private long points;

  public Statistics() {
	this.all = new long[]{0, 0, 0};
	this.daily = new long[]{0, 0, 0};
	this.weekly = new long[]{0, 0, 0};
  }

  public long[] getAll() {
	return all;
  }

  public void setAll(long[] all) {
	this.all = all;
  }

  public long[] getDaily() {
	return daily;
  }

  public void setDaily(long[] daily) {
	this.daily = daily;
  }

  public long[] getWeekly() {
	return weekly;
  }

  public void setWeekly(long[] weekly) {
	this.weekly = weekly;
  }

  public long getSizeActiveVocabulary() {
	return sizeActiveVocabulary;
  }

  public void setSizeActiveVocabulary(long sizeActiveVocabulary) {
	this.sizeActiveVocabulary = sizeActiveVocabulary;
  }

  /**
   * new words = 0
   * learning words = 1
   * known words = 5
   * imported script = 5
   * read script = 15
   *
   * @return calculated points
   */
  public long getPoints() {
	points = all[LEARNING_WORDS] * 2
			+ all[KNOWN_WORDS] * 5
			+ all[COUNT_OF_IMPORTED_SCRIPTS] * 5
			+ all[COUNT_OF_READ_SCRIPTS] * 15;
	return points;
  }

  @Override
  public String toString() {
	return "Statistics{" +
			"all=" + Arrays.toString(all) +
			", daily=" + Arrays.toString(daily) +
			", weekly=" + Arrays.toString(weekly) +
			", sizeActiveVocabulary=" + sizeActiveVocabulary +
			'}';
  }
}
