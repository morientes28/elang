package sk.morione.elango.script.model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@Entity
public class Tag implements Serializable {

  @Id
  private String name;

  @ManyToMany(mappedBy = "tags")
  private List<Translation> translations = new ArrayList<>();

  public Tag(String name) {
	this.name = name;
  }

  public String getName() {
	return name;
  }

  public void setName(String name) {
	this.name = name;
  }

  public List<Translation> getTranslations() {
	return translations;
  }

  public void setTranslations(List<Translation> translations) {
	this.translations = translations;
  }

  @Override
  public boolean equals(Object o) {
	if (this == o) return true;
	if (o == null || getClass() != o.getClass()) return false;
	Tag tag = (Tag) o;
	return Objects.equals(name, tag.name);
  }

  @Override
  public int hashCode() {
	return Objects.hash(name);
  }
}
