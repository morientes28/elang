package sk.morione.elango.script.model;

import org.springframework.lang.NonNull;
import sk.morione.elango.core.model.Base;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 30/09/2018.
 */
@Entity
@SequenceGenerator(name = "seq", allocationSize = 100)
public class Translation extends Base implements Serializable {

  @Id
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
  private long id;

  @NonNull
  private String origin;
  private String transcript;
  private String note;

  @ManyToMany(cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  @JoinTable(name = "translation_tag",
		  joinColumns = @JoinColumn(name = "translation_id"),
		  inverseJoinColumns = @JoinColumn(name = "tag_id")
  )
  private List<Tag> tags = new ArrayList<>();

  @NonNull
  private Evaluation evaluation = Evaluation.NEW;

  private Language transcriptLanguage;

  @ManyToOne(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  @JoinColumn(name = "script_id")
  private Script script;

  @ManyToOne(fetch = FetchType.EAGER, cascade = {CascadeType.MERGE, CascadeType.PERSIST})
  @JoinColumn(name = "corpus_id")
  private Corpus corpus;

  public Translation() {
  }

  public Translation(String origin) {
	this.origin = origin;
  }

  public Translation(String origin, Script script) {
	this.origin = origin;
	this.script = script;
  }

  public long getId() {
	return id;
  }

  public void setId(long id) {
	this.id = id;
  }

  public String getOrigin() {
	return origin;
  }

  public void setOrigin(String origin) {
	this.origin = origin;
  }

  public String getTranscript() {
	return transcript;
  }

  public void setTranscript(String transcript) {
	this.transcript = transcript;
  }

  public String getNote() {
	return note;
  }

  public void setNote(String note) {
	this.note = note;
  }

  public Evaluation getEvaluation() {
	return evaluation;
  }

  public void setEvaluation(Evaluation evaluation) {
	this.evaluation = evaluation;
  }

  public Language getTranscriptLanguage() {
	return transcriptLanguage;
  }

  public void setTranscriptLanguage(Language transcriptLanguage) {
	this.transcriptLanguage = transcriptLanguage;
  }

  public Script getScript() {
	return script;
  }

  public void setScript(Script script) {
	this.script = script;
  }

  public Corpus getCorpus() {
	return corpus;
  }

  public void setCorpus(Corpus corpus) {
	this.corpus = corpus;
  }

  public List<Tag> getTags() {
	return tags;
  }

  public void setTags(List<Tag> tags) {
	this.tags = tags;
  }

  public void addTag(Tag tag) {
	tags.add(tag);
	tag.getTranslations().add(this);
  }

  public void addTags(Tag... addTags) {
	for (Tag tag : addTags) {
	  tags.add(tag);
	  tag.getTranslations().add(this);
	}
  }

  public void removeTag(Tag tag) {
	tags.remove(tag);
	tag.getTranslations().remove(this);
  }

  @Override
  public boolean equals(Object o) {
	if (this == o) return true;
	if (!(o instanceof Translation)) return false;
	Translation that = (Translation) o;
	return id == that.id &&
			Objects.equals(origin, that.origin) &&
			Objects.equals(transcript, that.transcript);
  }

  @Override
  public int hashCode() {
	return 31;
  }
}
