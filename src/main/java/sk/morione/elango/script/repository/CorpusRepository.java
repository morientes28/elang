package sk.morione.elango.script.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sk.morione.elango.access.model.User;
import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.model.Language;

import java.util.List;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 30/09/2018.
 */
@Repository
public interface CorpusRepository extends JpaRepository<Corpus, Long> {

  List<Corpus> findByLanguage(Language language);

  Corpus findOneById(long id);

  Corpus findOneByLanguageAndBaseAndName(Language language, boolean base, String name);

  Corpus findOneByUserAndLanguage(User user, Language language);

}
