package sk.morione.elango.script.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import sk.morione.elango.script.model.Lesson;

import java.util.List;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 30/09/2018.
 */
@Repository
public interface LessonRepository extends JpaRepository<Lesson, Long> {

  List<Lesson> findAllByOrderByIdAsc();
}
