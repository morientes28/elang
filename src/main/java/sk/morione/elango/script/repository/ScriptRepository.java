package sk.morione.elango.script.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sk.morione.elango.script.model.Script;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 30/09/2018.
 */
@Repository
public interface ScriptRepository extends PagingAndSortingRepository<Script, Long> {

  Script findOneByLessonIdAndPage(long lesson, int page);

  @Override
  Page<Script> findAll(Pageable pageable);

  @Query(value = "SELECT * FROM Script s " +
		  "LEFT OUTER JOIN script_users su on su.script_id=s.id " +
		  "LEFT JOIN users u on u.id=su.user_id " +
		  "WHERE upper(s.title) like upper(concat('%', :title,'%')) " +
		  "AND s.language =:lang " +
		  "AND s.owner_id=:userId " +
		  "AND (su.script_id IS NULL OR u.id<>:userId)", nativeQuery = true)
  Page<Script> findAllUnreadPrivate(
		  @Param("title") String title,
		  @Param("lang") long lang,
		  @Param("userId") Long userId,
		  Pageable pageable);

  @Query(value = "SELECT * FROM Script s " +
		  "RIGHT JOIN script_users su on su.script_id=s.id " +
		  "RIGHT JOIN users u on u.id=su.user_id " +
		  "WHERE upper(s.title) like upper(concat('%', :title,'%')) " +
		  "AND s.language =:lang " +
		  "AND s.owner_id=:userId " +
		  "AND u.id=:userId", nativeQuery = true)
  Page<Script> findAllReadPrivate(
		  @Param("title") String title,
		  @Param("lang") long lang,
		  @Param("userId") Long userId,
		  Pageable pageable);

  @Query(value = "SELECT * FROM Script s " +
		  "LEFT OUTER JOIN script_users su on su.script_id=s.id " +
		  "LEFT JOIN users u on u.id=su.user_id " +
		  "WHERE upper(s.title) like upper(concat('%', :title,'%')) " +
		  "AND s.language =:lang " +
		  "AND s.owner_id IS NULL " +
		  "AND (su.script_id IS NULL OR u.id <>:userId)"
		  , nativeQuery = true)
  Page<Script> findAllUnreadPublic(
		  @Param("title") String title,
		  @Param("lang") long lang,
		  @Param("userId") Long userId,
		  Pageable pageable);

  @Query(value = "SELECT * FROM Script s " +
		  "RIGHT JOIN script_users su on su.script_id=s.id " +
		  "RIGHT JOIN users u on u.id=su.user_id " +
		  "WHERE upper(s.title) like upper(concat('%', :title,'%')) " +
		  "AND s.language =:lang " +
		  "AND s.owner_id IS NULL " +
		  "AND u.id=:userId", nativeQuery = true)
  Page<Script> findAllReadPublic(
		  @Param("title") String title,
		  @Param("lang") long lang,
		  @Param("userId") Long userId,
		  Pageable pageable);

  @Query(value = "SELECT * FROM Script s " +
		  "LEFT OUTER JOIN script_users su on su.script_id=s.id " +
		  "LEFT JOIN users u on u.id=su.user_id " +
		  "WHERE upper(s.title) like upper(concat('%', :title,'%')) " +
		  "AND s.language =:lang " +
		  "AND (s.owner_id IS NULL OR s.owner_id=:userId) " +
		  "AND (su.script_id IS NULL  OR u.id <>:userId)", nativeQuery = true)
  Page<Script> findAllUnreadPublicAndPrivate(
		  @Param("title") String title,
		  @Param("lang") long lang,
		  @Param("userId") Long userId,
		  Pageable pageable);

  @Query(value = "SELECT * FROM Script s " +
		  "RIGHT JOIN script_users su on su.script_id=s.id " +
		  "RIGHT JOIN users u on u.id=su.user_id " +
		  "WHERE upper(s.title) like upper(concat('%', :title,'%')) " +
		  "AND s.language =:lang " +
		  "AND (s.owner_id IS NULL OR s.owner_id=:userId) " +
		  "AND u.id=:userId", nativeQuery = true)
  Page<Script> findAllReadPublicAndPrivate(
		  @Param("title") String title,
		  @Param("lang") long lang,
		  @Param("userId") Long userId,
		  Pageable pageable);
}
