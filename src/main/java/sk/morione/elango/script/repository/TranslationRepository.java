package sk.morione.elango.script.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.model.Evaluation;
import sk.morione.elango.script.model.Script;
import sk.morione.elango.script.model.Translation;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 30/09/2018.
 */
@Repository
public interface TranslationRepository extends PagingAndSortingRepository<Translation, Long> {

  //TODO: try to make not native query
  @Query(value = "SELECT * FROM translation t " +
		  "LEFT JOIN translation_tag tt ON tt.translation_id=t.id " +
		  "LEFT JOIN tag g ON g.name=tt.tag_id WHERE g.name IN :tags", nativeQuery = true)
  Set<Translation> findAllByTags(@Param("tags") List<String> tags);

  Set<Translation> findAllByCorpusOrderByIdAsc(Corpus corpus);

  @Query(value = "SELECT t FROM Translation t WHERE " +
		  "t.corpus=:corpus AND t.script.title like :scriptTitle " +
		  "AND t.evaluation IN (:evaluations)")
  Page<Translation> findAllBySearchForm(
		  @Param("corpus") Corpus corpus,
		  @Param("scriptTitle") String scriptTitle,
		  @Param("evaluations") List<Evaluation> evaluations,
		  Pageable pageable);

  @Query(value = "SELECT t FROM Translation t WHERE " +
		  "t.corpus=:corpus AND t.script.title like :scriptTitle")
  Page<Translation> findAllBySearchForm(
		  @Param("corpus") Corpus corpus,
		  @Param("scriptTitle") String scriptTitle,
		  Pageable pageable);

  Set<Translation> findAllByScript(Script script);

  Set<Translation> findByOriginAndCorpus(String origin, Corpus corpus);

  Set<Translation> findByOrigin(String origin);

  @Query(value = "SELECT t FROM Translation t WHERE " +
		  "t.corpus=:corpus AND t.evaluation IN (:evaluations)")
  Set<Translation> findAllByCorpusAndEvaluationsOrderByIdAsc(
		  @Param("corpus") Corpus corpus,
		  @Param("evaluations") List<Evaluation> evaluations);

  @Modifying
  @Transactional
  @Query("UPDATE Translation t SET t.evaluation = :evaluation WHERE t.id = :id")
  void changeEvaluation(@Param("id") long id, @Param("evaluation") Evaluation evaluation);

  Optional<Translation> findById(long id);

  @Query(nativeQuery = true,
		  value = "SELECT TOP 1 * FROM translation t " +
				  "LEFT JOIN corpus c on c.id = t.corpus_id " +
				  "LEFT JOIN users u on u.id=c.user_id " +
				  "WHERE u.username=:username ORDER BY t.id DESC")
  Translation findLastByUsername(@Param("username") String username);

  int countAllByCorpus(Corpus corpus);
}
