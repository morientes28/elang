package sk.morione.elango.script.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sk.morione.elango.core.controller.BaseController;
import sk.morione.elango.core.model.RestResponse;
import sk.morione.elango.script.model.Script;
import sk.morione.elango.script.repository.ScriptRepository;
import sk.morione.elango.script.service.MatcherService;
import sk.morione.elango.script.service.TranslationService;
import sk.morione.elango.storage.service.FileProcessService;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@RestController
public class ScriptRestController extends BaseController {

  private final Logger log = LoggerFactory.getLogger(ScriptRestController.class);

  @Autowired
  FileProcessService fileProcessService;

  @Autowired
  TranslationService translationService;

  @Autowired
  ScriptRepository scriptRepository;

  @Autowired
  MatcherService matcherService;

  @PostMapping(value = "/script/change", produces = "application/json")
  @ResponseBody
  public RestResponse changeScript(@RequestBody Script script) {

	Script dbscript = translationService.getScriptById(script.getId());

	if (StringUtils.hasText(script.getTitle()))
	  dbscript.setTitle(script.getTitle());
	if (StringUtils.hasText(script.getContent()))
	  dbscript.setContent(script.getContent());
	if (StringUtils.hasText(script.getEmbedUrl()))
	  dbscript.setEmbedUrl(script.getEmbedUrl());
	dbscript.setLevel(script.getLevel());

	scriptRepository.save(dbscript);

	log.debug("Saved script [{}]", script.getId());

	return RestResponse.create(script.getId());
  }

  @PostMapping("/script/image/{id}")
  @ResponseBody
  public RestResponse uploadImage(@PathVariable("id") Long id,
								  @RequestParam("image") MultipartFile image) {

	String result = "";
	if (!image.isEmpty()) {
	  result = translationService.saveImageToScript(id, image);
	  log.info("Image {} has been saved", image.getOriginalFilename());
	}

	return RestResponse.create(result);
  }

}
