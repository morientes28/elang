package sk.morione.elango.script.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import sk.morione.elango.core.controller.BaseController;
import sk.morione.elango.core.model.RestResponse;
import sk.morione.elango.core.model.TypeActivity;
import sk.morione.elango.core.service.ActivityService;
import sk.morione.elango.script.model.Script;
import sk.morione.elango.script.model.Translation;
import sk.morione.elango.script.service.TranslationService;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.Set;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@RestController
public class TranslationRestController extends BaseController {

  private final Logger log = LoggerFactory.getLogger(TranslationRestController.class);
  @Autowired
  TranslationService service;

  @Autowired
  ActivityService activityService;


  @PostMapping(value = "/translation", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public Translation getTranslation(@RequestBody Translation text) {

	Translation tmp = findTransaction(text.getOrigin());

	// don't want to have relations, only base translation
	tmp.setCorpus(null);
	tmp.setScript(null);
	log.debug("Getting transaction [{}]", tmp);

	return tmp;
  }

  /**
   * Try to find transaction in user corpus. In worse case try to find transaction in all DB.
   *
   * @param origin
   * @return Transaction
   */
  private Translation findTransaction(String origin) {

	Translation tmp = new Translation();
	Set<Translation> listFinded = translationRepository.findByOrigin(origin);

	if (!listFinded.isEmpty()) {
	  tmp = listFinded
			  .stream()
			  .filter(i ->
					  (i.getCorpus() != null) && i.getCorpus().equals(activeProfile().getSelectedCorpus()))
			  .findFirst().orElse(listFinded.iterator().next());
	}
	return tmp;
  }

  @PostMapping("/translation/change")
  @Transactional
  @ResponseBody
  public RestResponse changeTranslation(@RequestBody Translation translation) {

	Translation translation1 = new Translation();
	if (translation.getId() > 0) {
	  translation1 = translationRepository.findById(translation.getId()).orElseThrow(EntityNotFoundException::new);
	} else {
	  Script script = service.getScriptById(translation.getScript().getId());
	  // avoid duplicity in corpus
	  Set<Translation> duplicity = translationRepository.findByOriginAndCorpus(
			  translation.getOrigin(), activeProfile().getSelectedCorpus());
	  if (!duplicity.isEmpty()) {
		translation1.setId(duplicity.iterator().next().getId());
	  }
	  translation1.setScript(script);
	  translation1.setCorpus(activeProfile().getSelectedCorpus());
	}

	if (translation.getOrigin() != null)
	  translation1.setOrigin(translation.getOrigin());
	if (translation.getTranscript() != null)
	  translation1.setTranscript(translation.getTranscript());

	translation1.setNote(translation.getNote());
	translation1.setEvaluation(translation.getEvaluation());

	if (translation.getScript()!=null) {
	  Script script = scriptRepository.findById(translation.getScript().getId())
			  .orElseThrow(EntityNotFoundException::new);
	  translation1.setScript(script);
	}
	translationRepository.save(translation1);

	activityService.activity(
			activeProfile().getUsername(),
			TypeActivity.getTypeActivityByEvaluation(translation.getEvaluation()),
			String.valueOf(translation1.getId() + "_" + translation1.getEvaluation().name()),
			activeProfile().getSelectedCorpus());

	log.debug("Updated translation [{}]", translation1);

	return RestResponse.create(translation1.getId());
  }

  @DeleteMapping("/translation/{id}")
  @Transactional
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void deleteTranslation(@PathVariable Long id) {

	service.deleteTranslationById(id);
	log.debug("Deleted translation [{}]", id);

  }

}
