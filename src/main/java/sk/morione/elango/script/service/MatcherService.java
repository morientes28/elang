package sk.morione.elango.script.service;

import org.springframework.stereotype.Service;
import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.model.Script;
import sk.morione.elango.script.model.Translation;

import javax.transaction.Transactional;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@Service
public class MatcherService {

  final static private String NAMES = "Oliver,Jake,Noah,James,Jack,Connor,Liam,John,Harry,Callum,Mason,Robert,Jacob," +
		  "Michael,Charlie,Kyle,William,William,Thomas,Joe,Ethan,David,George,Reece,Richard," +
		  "Oscar,Rhys,Alexander,Joseph,James,Charlie,James,Charles,William,Damian,Daniel,Thomas," +
		  "Amelia,Margaret,Emma,Mary,Olivia,Samantha,Olivia,Patricia,Isla,Bethany,Sophia,Jennifer," +
		  "Emily,Elizabeth,Isabella,Elizabeth,Poppy,Joanne,Ava,Linda,Ava,Megan,Mia,Barbara," +
		  "Isabella,Victoria,Susan,Jessica,Lauren,Abigail,Margaret,Lily,Michelle,Madison,Jessica," +
		  "Sophie,Tracy,Charlotte,Sarah,Murphy,Li,Jones,O'Kelly,Johnson,Williams,Sullivan,Lam," +
		  "Brown,Walsh,Taylor,Smith,Gelbero,Davies,O'Brien,Miller,Roy,Byrne,Davis,Tremblay," +
		  "Morton,Singh,Evans,O'Ryan,Garcia,Lee,White,Wang,Thomas,O'Connor,Rodriguez,Gagnon," +
		  "Martin,Roberts,O'Neill,Wilson,Anderson,Ross,Peter,Albert,Steve,Berlin,Bratislava,Prague";
  final static private List<String> NAMELIST = Arrays.asList(NAMES.toLowerCase().split(","));
  final static private String CITIES = "Paris,London,Bangkok,Singapore,York,Lumpur,Kong,Dubai,Istanbul,Rome,Shanghai," +
		  "Vegas,Miami,Toronto,Barcelona,Dublin,Amsterdam,Moscow,Cairo,Prague,Vienna,Madrid," +
		  "Francisco,Vancouver,Budapest,Janeiro,Berlin,Tokyo,Mexico,Aires,Petersburg,Seoul,Athens," +
		  "Jerusalem,Seattle,Delhi,Sydney,Mumbai,Munich,Venice,Florence,Beijing,Washington," +
		  "Montreal,Atlanta,Boston,Philadelphia,Chicago,Stockholm,Cancún,Warsaw,Dallas,Milan," +
		  "Oslo,Libson,Johannesburg,Antalya,Mecca,Macau,Pattaya,Guangzhou,Kiev,Shenzhen,Bucharest," +
		  "Taipei,Orlando,Brussels,Chennai,Marrakesh,Phuket,Edirne,Bali,Copenhagen,Agra,Varna," +
		  "Riyadh,Jakarta,Auckland,Honolulu,Edinburgh,Wellington,Orleans,Petra,Melbourne,Luxor," +
		  "Manila,Houston,Phnom,Zürich,Lima,Santiago,Bogotá";
  final static private List<String> CITIESLIST = Arrays.asList(CITIES.toLowerCase().split(","));

  private static List<String> filter(List<String> words) {

	return words.stream()
			.filter(MatcherService::withoutNames)
			.filter(MatcherService::withoutCities)
			.collect(Collectors.toList());
  }

  private static boolean withoutNames(String word) {
	return !NAMELIST.contains(word);
  }

  private static boolean withoutCities(String word) {
	return !CITIESLIST.contains(word);
  }

  @Transactional
  public Set<String> findUnknownWords(Script script, Corpus corpus) {

	List<String> result = new ArrayList<>();
	for (String word : script.getWordsFromContent()) {
	  boolean flag = true;
	  for (Translation translation : corpus.getTranslations()) {
		if (translation.getOrigin() == null)
		  continue;
		if (check(translation, word))
		  flag = false;
	  }
	  if (flag)
		result.add(word);
	}
	return new HashSet<>(filter(result));
  }

  private boolean check(Translation translation, String word) {
	return translation.getOrigin().toLowerCase().equals(word);
  }

}
