package sk.morione.elango.script.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import sk.morione.elango.script.form.SearchScriptForm;
import sk.morione.elango.script.model.Script;
import sk.morione.elango.script.repository.ScriptRepository;

import javax.transaction.Transactional;


/**
 * Created by  ⌐⨀_⨀¬ morientes on 29/10/2018.
 */
@Service
@CacheConfig(cacheNames = "scripts")
public class ScriptService {

  private final Logger log = LoggerFactory.getLogger(ScriptService.class);

  @Autowired
  ScriptRepository scriptRepository;

  @CacheEvict(allEntries = true)
  public void clearCache() {
	log.info("Clear cache namespace: 'scripts'");
  }



  @Transactional
  @Cacheable
  public Page<Script> getByCritery(SearchScriptForm form, PageRequest pageRequest) {

	//TODO: maybe better to user QueryDSL [http://www.querydsl.com/index.html]

	if (!form.isRead()) {
	  if (form.isPriv() && form.isPubl()) {
		// all private and public
		return scriptRepository.findAllUnreadPublicAndPrivate(
				form.getText(),
				form.getLanguage().getId(),
				form.getUserId(),
				pageRequest);

	  } else if (form.isPriv() && !form.isPubl()) {
		// only private
		return scriptRepository.findAllUnreadPrivate(
				form.getText(),
				form.getLanguage().getId(),
				form.getUserId(),
				pageRequest);

	  } else if (!form.isPriv() && form.isPubl()) {
		// only public
		return scriptRepository.findAllUnreadPublic(
				form.getText(),
				form.getLanguage().getId(),
				form.getUserId(),
				pageRequest);
	  }
	} else {
	  // is read
	  if (form.isPriv() && form.isPubl()) {
		// all private and public
		return scriptRepository.findAllReadPublicAndPrivate(
				form.getText(),
				form.getLanguage().getId(),
				form.getUserId(),
				pageRequest);

	  } else if (form.isPriv() && !form.isPubl()) {
		// only private
		return scriptRepository.findAllReadPrivate(
				form.getText(),
				form.getLanguage().getId(),
				form.getUserId(),
				pageRequest);

	  } else if (!form.isPriv() && form.isPubl()) {
		// only public
		return scriptRepository.findAllReadPublic(
				form.getText(),
				form.getLanguage().getId(),
				form.getUserId(),
				pageRequest);
	  }
	}

	return Page.empty();
  }
}
