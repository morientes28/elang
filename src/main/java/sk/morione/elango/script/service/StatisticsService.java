package sk.morione.elango.script.service;

import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.morione.elango.access.model.Profile;
import sk.morione.elango.access.model.User;
import sk.morione.elango.access.repository.UserRepository;
import sk.morione.elango.core.helper.Helper;
import sk.morione.elango.core.model.TypeActivity;
import sk.morione.elango.core.repository.ActivityRepository;
import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.model.Statistics;
import sk.morione.elango.script.repository.TranslationRepository;

import java.time.LocalDate;
import java.time.ZoneId;


/**
 * Created by  ⌐⨀_⨀¬ morientes on 12/11/2018.
 */
@Service
public class StatisticsService {

  private final Logger log = LoggerFactory.getLogger(StatisticsService.class);

  @Autowired
  TranslationRepository repository;

  @Autowired
  ActivityRepository activityRepository;

  @Autowired
  UserRepository userRepository;

  public Statistics getStatistics(Profile profile) {

	Statistics result = new Statistics();
	Corpus corpus = profile.getSelectedCorpus();
	LocalDate today = LocalDate.now(ZoneId.of("Europe/Berlin"));
	LocalDate weekAgo = today.minusDays(7);
	User user = userRepository.findByUsername(profile.getUsername());

	long[] all = new long[Statistics.SIZE];

	Stopwatch stopwatch = Helper.watchTime();
	all[Statistics.NEW_WORDS] = activityRepository.countActivitiesByUserAndTypeAndCorpus(
			user, TypeActivity.EVALUATED_NEW, profile.getSelectedCorpus());
	all[Statistics.LEARNING_WORDS] = activityRepository.countActivitiesByUserAndTypeAndCorpus(
			user, TypeActivity.EVALUATED_LEARNING, profile.getSelectedCorpus());
	all[Statistics.KNOWN_WORDS] = activityRepository.countActivitiesByUserAndTypeAndCorpus(
			user, TypeActivity.EVALUATED_KNOWN, profile.getSelectedCorpus());
	all[Statistics.COUNT_OF_IMPORTED_SCRIPTS] = activityRepository.countActivitiesByUserAndTypeAndCorpus(
			user, TypeActivity.IMPORT_SCRIPT, profile.getSelectedCorpus());
	all[Statistics.COUNT_OF_READ_SCRIPTS] = activityRepository.countActivitiesByUserAndTypeAndCorpus(
			user, TypeActivity.READ_SCRIPT, profile.getSelectedCorpus());

	Helper.watchTime(stopwatch, "All statistics");


	stopwatch = Helper.watchTime();
	long[] daily = new long[Statistics.SIZE];

	daily[Statistics.NEW_WORDS] = activityRepository.countActivitiesByUserAndTypeAndCorpusAndCreatedOnAfter(
			user, TypeActivity.EVALUATED_NEW, profile.getSelectedCorpus(), today.atStartOfDay());
	daily[Statistics.LEARNING_WORDS] = activityRepository.countActivitiesByUserAndTypeAndCorpusAndCreatedOnAfter(
			user, TypeActivity.EVALUATED_LEARNING, profile.getSelectedCorpus(), today.atStartOfDay());
	daily[Statistics.KNOWN_WORDS] = activityRepository.countActivitiesByUserAndTypeAndCorpusAndCreatedOnAfter(
			user, TypeActivity.EVALUATED_KNOWN, profile.getSelectedCorpus(), today.atStartOfDay());
	daily[Statistics.COUNT_OF_IMPORTED_SCRIPTS] = activityRepository.countActivitiesByUserAndTypeAndCorpusAndCreatedOnAfter(
			user, TypeActivity.IMPORT_SCRIPT, profile.getSelectedCorpus(), today.atStartOfDay());
	daily[Statistics.COUNT_OF_READ_SCRIPTS] = activityRepository.countActivitiesByUserAndTypeAndCorpusAndCreatedOnAfter(
			user, TypeActivity.READ_SCRIPT, profile.getSelectedCorpus(), today.atStartOfDay());

	Helper.watchTime(stopwatch, "Daily statistics");


	stopwatch = Helper.watchTime();
	long[] weekly = new long[Statistics.SIZE];
	weekly[Statistics.NEW_WORDS] = activityRepository.countActivitiesByUserAndTypeAndCorpusAndCreatedOnAfter(
			user, TypeActivity.EVALUATED_NEW, profile.getSelectedCorpus(), weekAgo.atStartOfDay());
	weekly[Statistics.LEARNING_WORDS] = activityRepository.countActivitiesByUserAndTypeAndCorpusAndCreatedOnAfter(
			user, TypeActivity.EVALUATED_LEARNING, profile.getSelectedCorpus(), weekAgo.atStartOfDay());
	weekly[Statistics.KNOWN_WORDS] = activityRepository.countActivitiesByUserAndTypeAndCorpusAndCreatedOnAfter(
			user, TypeActivity.EVALUATED_KNOWN, profile.getSelectedCorpus(), weekAgo.atStartOfDay());
	weekly[Statistics.COUNT_OF_IMPORTED_SCRIPTS] = activityRepository.countActivitiesByUserAndTypeAndCorpusAndCreatedOnAfter(
			user, TypeActivity.IMPORT_SCRIPT, profile.getSelectedCorpus(), weekAgo.atStartOfDay());
	weekly[Statistics.COUNT_OF_READ_SCRIPTS] = activityRepository.countActivitiesByUserAndTypeAndCorpusAndCreatedOnAfter(
			user, TypeActivity.READ_SCRIPT, profile.getSelectedCorpus(), weekAgo.atStartOfDay());

	Helper.watchTime(stopwatch, "Weekly statistics");

	result.setAll(all);
	result.setDaily(daily);
	result.setWeekly(weekly);
	result.setSizeActiveVocabulary(repository.countAllByCorpus(corpus));

	return result;
  }


}
