package sk.morione.elango.script.service;

import com.google.cloud.translate.Translate;
import com.google.cloud.translate.TranslateOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import sk.morione.elango.access.model.Profile;
import sk.morione.elango.access.model.User;
import sk.morione.elango.access.repository.UserRepository;
import sk.morione.elango.core.config.LoadEssentialBean;
import sk.morione.elango.core.config.ServiceConfig;
import sk.morione.elango.core.model.Scheduler;
import sk.morione.elango.core.repository.SchedulerRepository;
import sk.morione.elango.script.form.ImportForm;
import sk.morione.elango.script.model.*;
import sk.morione.elango.script.repository.CorpusRepository;
import sk.morione.elango.script.repository.LessonRepository;
import sk.morione.elango.script.repository.ScriptRepository;
import sk.morione.elango.script.repository.TranslationRepository;
import sk.morione.elango.storage.service.FileProcessService;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.*;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@Service
public class TranslationService {

  private final Logger log = LoggerFactory.getLogger(TranslationService.class);
  @Autowired
  ScriptRepository scriptRepository;

  @Autowired
  LessonRepository lessonRepository;

  @Autowired
  UserRepository userRepository;

  @Autowired
  CorpusRepository corpusRepository;

  @Autowired
  TranslationRepository translationRepository;

  @Autowired
  SchedulerRepository schedulerRepository;

  @Autowired
  MatcherService service;

  @Autowired
  ServiceConfig config;

  @Autowired
  LoadEssentialBean loadEssentialBean;

  @Autowired
  FileProcessService fileProcessService;

  public List<Lesson> getAllLessonSortByCreatedOn() {
	return lessonRepository.findAllByOrderByIdAsc();
  }

  @Transactional
  public Script getScriptById(long id) {
	Optional<Script> result = scriptRepository.findById(id);
	return result.orElseThrow(EntityNotFoundException::new);
  }

  @Transactional
  public void deleteScriptById(long id) {

	Script script = scriptRepository.findById(id).orElseThrow(IllegalStateException::new);

	//FIXME: try to use cascadeType annotation on object than particulary detach entity
	Set<Translation> translations = translationRepository.findAllByScript(script);
	translations.forEach((t) -> t.setScript(null));
	translationRepository.saveAll(translations);
	script.getUsers().forEach((u)->u.getScripts().remove(script));
	scriptRepository.deleteById(id);
	log.debug("Deleted script [{}]", script);
  }

  @Transactional
  public void unreadScriptByIdAndProfile(long id, Profile profile) {

	Script script = scriptRepository.findById(id).orElseThrow(IllegalStateException::new);
	User user = userRepository.findByUsername(profile.getUsername());
	script.removeUser(user);
	scriptRepository.save(script);
	log.debug("Unread script [{}] has been set to user [{}]", script, user);
  }

  @Transactional
  public String saveImageToScript(long id, MultipartFile image) {

	Script dbscript = getScriptById(id);
	if (StringUtils.hasText(dbscript.getMedia())) {
	  fileProcessService.amazonClient.deleteFileFromS3Bucket(dbscript.getMedia());
	}
	String newFileUrl = fileProcessService.processFile(image);
	dbscript.setMedia(newFileUrl);
	scriptRepository.save(dbscript);
	log.info("New image [{}] has been upload to script {}", newFileUrl, dbscript);

	return dbscript.getMedia();
  }

  @Transactional
  public Script getScriptByLessonAndPage(long lesson, int id) {
	return scriptRepository.findOneByLessonIdAndPage(lesson, id);
  }

  @Transactional
  public long saveScript(String text, Corpus corpus, ImportForm form, String imageUrl) {

	Script script = new Script(text, form.getTitle(), imageUrl);
	script.setLevel(Level.valueOf(new Long(form.getLevel())));
	script.setEmbedUrl(form.getEmbedUrl());
	script.setLanguage(corpus.getLanguage());

	//FIXME: refactor to something what will give a sense
	// created script by superuser will not set a user
	if (form.getUserId() != null) {
	  User user = userRepository.findById(
			  form.getUserId()).orElseThrow(EntityNotFoundException::new);
	  script.addUser(user);
	  script.setOwner(user);
	} else
	  script.setProcessed(true);
	script = scriptRepository.save(script);

	if (form.getUserId() != null)
	  addTranslationToCorpus(script, corpus);

	log.info("New script has been created [{}]", script);

	return script.getId();

  }

  @Transactional
  public void addTranslationToCorpus(Script script, Corpus corpus) {

	Set<String> newOrigins = service.findUnknownWords(script, corpus);
	newOrigins.forEach(log::info);

	if (newOrigins.size() > 0) {
	  Scheduler scheduler = new Scheduler(script, corpus, newOrigins);
	  schedulerRepository.save(scheduler);
	  log.info("Scheduled a job for {} number of translations [ script {} and corpus {} ]",
			  newOrigins.size(), script, corpus);
	}
  }

  @Transactional
  public void deleteTranslationById(long id) {
	Translation translation = translationRepository.findById(id).orElseThrow(EntityNotFoundException::new);
	//TODO: find out why I have to detach script and corpus in order to delete translation
	translation.setScript(null);
	translation.setCorpus(null);
	translationRepository.delete(translation);
  }

  public Page<Translation> randomTransation(long count, Page<Translation> translations) {

	Long size = translations.getTotalElements();
	Set<Translation> result = new HashSet<>(new ArrayList<>());

	if (count >= size)
	  count = size;

	while (result.size() != count) {
	  int item = new Random().nextInt(size.intValue());
	  int i = 0;
	  for (Translation obj : translations) {
		if (i == item)
		  result.add(obj);
		i++;
	  }
	}
	return new PageImpl<>(new ArrayList<>(result));
  }

  public void makeTranslation(String origin, Corpus corpus, Script script) {
	Translation translation = new Translation(origin);
	//TODO: right now only hardcoded SK target translation
	translation.setTranscript(translate(origin, corpus.getLanguage(), Language.SK));
	translation.setCorpus(corpus);
	translation.setScript(script);
	if (corpus.addTranslation(translation))
	  corpusRepository.save(corpus);
  }

  public Set<Translation> getAllUnknownTransactionsByCorpus(Corpus corpus) {
	return translationRepository.findAllByCorpusAndEvaluationsOrderByIdAsc(
			corpus, Arrays.asList(Evaluation.NEW, Evaluation.LEARNING));
  }

  public void addScriptToUser(Script script, Profile profile) {
	User user = userRepository.findByUsername(profile.getUsername());
	if (!script.getUsers().contains(user)) {
	  script.addUser(user);
	  log.info("Add user {} to script {}", user, script);
	  scriptRepository.save(script);
	  addTranslationToCorpus(script, profile.getSelectedCorpus());
	}
  }

  private String translate(String origin, Language srcLanguage, Language tgtLanguage) {
	Optional<Translation> found = tryToFindInBaseCorpus(srcLanguage, origin);
	if (found.isPresent()) {
	  log.debug("Found {} in base {} corpus", origin, srcLanguage.name());
	  return found.get().getTranscript();
	}
	log.debug("Call GoogleAPI : [{}/{} - {}]", srcLanguage, tgtLanguage, origin);
	return googleTranslate(origin, srcLanguage, tgtLanguage);
  }

  protected String googleTranslate(String word, Language srcLanguage, Language tgtLanguage) {
	System.setProperty("GOOGLE_API_KEY", config.getGoogleApiKey());
	Translate translate = TranslateOptions.getDefaultInstance().getService();
	Translate.TranslateOption srcLang = Translate.TranslateOption.sourceLanguage(srcLanguage.name());
	Translate.TranslateOption tgtLang = Translate.TranslateOption.targetLanguage(tgtLanguage.name());
	return translate.translate(word, srcLang, tgtLang).getTranslatedText();
  }

  //TODO: performance - instead of connecting to DB use list of translation from in-memory cache
  private Optional<Translation> tryToFindInBaseCorpus(Language language, String origin) {
	switch (language) {
	  case EN:
		Set<Translation> found = translationRepository.findByOriginAndCorpus(
				origin, loadEssentialBean.englishBaseCorpus()
		);
		return !found.isEmpty() ? Optional.of(found.iterator().next()) : Optional.empty();
	  case SK:
		//TODO: future SK
		return Optional.empty();
	  case ES:
		//TODO: future ES
		return Optional.empty();
	  case IT:
		//TODO: future IT
		return Optional.empty();
	  case DE:
		//TODO: future DE
		return Optional.empty();
	  default:
		return Optional.empty();
	}
  }


}
