package sk.morione.elango.script.service;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp;
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver;
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow;
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.json.GoogleJsonResponseException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.store.FileDataStoreFactory;
import com.google.api.services.youtube.YouTube;
import com.google.api.services.youtube.YouTubeScopes;
import com.google.api.services.youtube.model.Channel;
import com.google.api.services.youtube.model.ChannelListResponse;

import java.io.*;
import java.util.Collections;
import java.util.List;

/**
 * @author ⌐⨀_⨀¬ morientes on 2018-11-30.
 */
public class YoutubeService {

  /**
   * Application name.
   */
  private static final String APPLICATION_NAME = "eLango";

  /**
   * Directory to store user credentials for this application.
   */
  private static final java.io.File DATA_STORE_DIR = new java.io.File(
		  System.getProperty("user.home"), ".credentials/youtube-java-api");
  /**
   * Global instance of the JSON factory.
   */
  private static final JsonFactory JSON_FACTORY =
		  JacksonFactory.getDefaultInstance();
  /**
   * Global instance of the scopes required by this quickstart.
   * <p>
   * If modifying these scopes, delete your previously saved credentials
   * at ~/.credentials/drive-java-quickstart
   */
  private static final List<String> SCOPES =
		  Collections.singletonList(YouTubeScopes.YOUTUBE_READONLY);
  /**
   * Global instance of the {@link FileDataStoreFactory}.
   */
  private static FileDataStoreFactory DATA_STORE_FACTORY;
  /**
   * Global instance of the HTTP transport.
   */
  private static HttpTransport HTTP_TRANSPORT;

  static {
	try {
	  HTTP_TRANSPORT = GoogleNetHttpTransport.newTrustedTransport();
	  DATA_STORE_FACTORY = new FileDataStoreFactory(DATA_STORE_DIR);
	} catch (Throwable t) {
	  t.printStackTrace();
	  System.exit(1);
	}
  }

  /**
   * Create an authorized Credential object.
   *
   * @return an authorized Credential object.
   * @throws IOException
   */
  public static Credential authorize() throws IOException {
	// Load client secrets.
	File initialFile = new File("/Users/morientes/Work/morione/elango/client_secret.json");
	InputStream in = new FileInputStream(initialFile);

	//InputStream in =
	//		YoutubeService.class.getResourceAsStream("/Users/morientes/Work/morione/elango/client_secret.json");
	GoogleClientSecrets clientSecrets =
			GoogleClientSecrets.load(JSON_FACTORY, new InputStreamReader(in));

	// Build flow and trigger user authorization request.
	GoogleAuthorizationCodeFlow flow =
			new GoogleAuthorizationCodeFlow.Builder(
					HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, SCOPES)
					.setDataStoreFactory(DATA_STORE_FACTORY)
					.setAccessType("offline")
					.build();
	Credential credential = new AuthorizationCodeInstalledApp(
			flow, new LocalServerReceiver()).authorize("user");
	return credential;
  }

  /**
   * Build and return an authorized API client service, such as a YouTube
   * Data API client service.
   *
   * @return an authorized API client service
   * @throws IOException
   */
  public static YouTube getYouTubeService() throws IOException {
	Credential credential = authorize();
	return new YouTube.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential)
			.setApplicationName(APPLICATION_NAME)
			.build();
  }

  public static void main(String[] args) throws IOException {
	YouTube youtube = getYouTubeService();
	try {
	  YouTube.Channels.List channelsListByUsernameRequest = youtube.channels().list("snippet,contentDetails,statistics");
	  channelsListByUsernameRequest.setForUsername("GoogleDevelopers");

	  ChannelListResponse response = channelsListByUsernameRequest.execute();
	  Channel channel = response.getItems().get(0);
	  System.out.printf(
			  "This channel's ID is %s. Its title is '%s', and it has %s views.\n",
			  channel.getId(),
			  channel.getSnippet().getTitle(),
			  channel.getStatistics().getViewCount());
	} catch (GoogleJsonResponseException e) {
	  e.printStackTrace();
	  System.err.println("There was a service error: " +
			  e.getDetails().getCode() + " : " + e.getDetails().getMessage());
	} catch (Throwable t) {
	  t.printStackTrace();
	}
  }
}
