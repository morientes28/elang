package sk.morione.elango.storage.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sk.morione.elango.core.model.RestResponse;
import sk.morione.elango.storage.service.AmazonClient;

/**
 * Rest controller provides API implementation for uploading and deleting file to AWS S3
 *
 * @author  ⌐⨀_⨀¬ morientes on 11/10/2018.
 */
@RestController
@RequestMapping("/storage/")
public class BucketRestController {

  public static final String AMAZON_BUCKET_DIR = "";
  private AmazonClient amazonClient;

  @Autowired
  BucketRestController(AmazonClient amazonClient) {
	this.amazonClient = amazonClient;
  }

  /**
   * Upload file to AWS S3
   *
   * @param file - multipart from client form
   * @return RestResponse with AWS S3 bucket URL uploaded file
   */
  @PostMapping("/uploadFile")
  @ResponseStatus(value = HttpStatus.CREATED)
  public RestResponse uploadFile(@RequestPart(value = "file") MultipartFile file) {
	String url = this.amazonClient.uploadFile(file, AMAZON_BUCKET_DIR);
	return RestResponse.create(url);
  }


  /**
   * Delete file from AWS S3
   * @param fileUrl - AWS S3 bucket URL
   */
  @DeleteMapping("/deleteFile")
  @ResponseStatus(value = HttpStatus.NO_CONTENT)
  public void deleteFile(@RequestPart(value = "url") String fileUrl) {
	this.amazonClient.deleteFileFromS3Bucket(fileUrl);
  }
}
