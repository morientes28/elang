package sk.morione.elango.storage.service;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.DeleteObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sk.morione.elango.core.config.ServiceConfig;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 11/10/2018.
 */
@Service
public class AmazonClient {

  private static final Logger log = LoggerFactory.getLogger(AmazonClient.class);
  private AmazonS3 s3client;

  @Autowired
  private ServiceConfig config;


  @PostConstruct
  private void initializeAmazon() {

	this.s3client = AmazonS3ClientBuilder.standard()
			.withRegion(Regions.EU_WEST_2).withForceGlobalBucketAccessEnabled(true)
			.build();
  }

  public String uploadFile(MultipartFile multipartFile, String dir) {
	return uploadFile(multipartFile, dir, null);
  }

  public String uploadFile(MultipartFile multipartFile, String dir,
						   FileProcessService fileProcessService) {

	String fileUrl = "";
	try {
	  File file = convertMultiPartToFile(multipartFile);

	  if (fileProcessService != null) {
		log.info("It is doing a compression of image");
		fileProcessService.compressAndScale(file);
	  }
	  String fileName = generateFileName(multipartFile);
	  fileUrl = config.getAmazonEndpointUrl() + "/"
			  + config.getAmazonBucketName() + dir + "/" + fileName;
	  uploadFileToS3bucket(fileName, file);
	  log.info("Call AWS: Upload file to S3 Bucket [{}]", fileUrl);
	  file.deleteOnExit();
	} catch (Exception e) {
	  e.printStackTrace();
	  log.error("Problem during uploading file {} to Amazon", multipartFile);
	}
	return fileUrl;
  }


  public String deleteFileFromS3Bucket(String fileUrl) {
	String fileName = fileUrl.substring(fileUrl.lastIndexOf("/") + 1);
	s3client.deleteObject(new DeleteObjectRequest(config.getAmazonBucketName() + "/", fileName));
	log.info("Call AWS: Delete file from S3 Bucket [{}]", fileUrl);
	return "Successfully deleted";
  }

  public void uploadFileToS3bucket(String fileName, File file) {
	s3client.putObject(new PutObjectRequest(config.getAmazonBucketName(), fileName, file)
			.withCannedAcl(CannedAccessControlList.PublicRead));
  }

  private String generateFileName(MultipartFile multiPart) {
	return new Date().getTime() + "-" + multiPart.getOriginalFilename().replace(" ", "_");
  }

  private File convertMultiPartToFile(MultipartFile file) throws IOException {
	File convFile = new File(file.getOriginalFilename());
	FileOutputStream fos = new FileOutputStream(convFile);
	fos.write(file.getBytes());
	fos.close();
	return convFile;
  }

}
