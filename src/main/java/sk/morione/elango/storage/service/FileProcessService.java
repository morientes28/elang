package sk.morione.elango.storage.service;

import com.tinify.Options;
import com.tinify.Source;
import com.tinify.Tinify;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import sk.morione.elango.core.config.ServiceConfig;
import sk.morione.elango.storage.rest.BucketRestController;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;

import static org.apache.http.entity.ContentType.*;


/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@Service
public class FileProcessService {

  private final Logger log = LoggerFactory.getLogger(FileProcessService.class);

  private final Tesseract tesseract = new Tesseract();
  @Autowired
  public AmazonClient amazonClient;
  private ServiceConfig serviceConfig;
  @Autowired
  private Environment env;

  @Autowired
  public FileProcessService(ServiceConfig serviceConfig) {

	this.serviceConfig = serviceConfig;
	tesseract.setDatapath(serviceConfig.getTesseractDataPath());
	Tinify.setKey(serviceConfig.getTinypngApikey());

  }

  private static String getUrlContents(String theUrl) {

	StringBuilder content = new StringBuilder();

	// many of these calls can throw exceptions, so i've just
	// wrapped them all in one try/catch statement.
	try {
	  // create a url object
	  URL url = new URL(theUrl);

	  // create a urlconnection object
	  URLConnection urlConnection = url.openConnection();

	  // wrap the urlconnection in a bufferedreader
	  BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

	  String line;

	  // read from the urlconnection via the bufferedreader
	  while ((line = bufferedReader.readLine()) != null) {
		content.append(line + "\n");
	  }
	  bufferedReader.close();

	} catch (Exception e) {

	  e.printStackTrace();

	}

	return content.toString();

  }

  public static File getUrlFile(String url, String destination) {

	try (BufferedInputStream in = new BufferedInputStream(new URL(url).openStream());

		 FileOutputStream fileOutputStream = new FileOutputStream(destination)) {
	  byte dataBuffer[] = new byte[1024];
	  int bytesRead;
	  while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
		fileOutputStream.write(dataBuffer, 0, bytesRead);
	  }
	} catch (IOException e) {
	  e.printStackTrace();
	}
	return new File(destination);
  }

  public String extractFromFileOrText(MultipartFile file, String text) {

	String ocrText = text;
	if (file != null && !file.isEmpty()) {
	  if (Objects.equals(file.getContentType(), TEXT_PLAIN.getMimeType())) {

		log.info("Start importing text file {}", file.getOriginalFilename());
		ocrText = getText(file);

	  } else if (Objects.equals(file.getContentType(), IMAGE_PNG.getMimeType())
			  || Objects.equals(file.getContentType(), IMAGE_JPEG.getMimeType())) {

		log.info("Start importing image file {}", file.getOriginalFilename());
		ocrText = ocr(file);
	  }
	  log.info("Imported text {}", ocrText);
	}

	return ocrText;

  }

  public String ocr(String filepath) {

	try {

	  return tesseract.doOCR(new File(filepath));

	} catch (TesseractException e) {

	  e.printStackTrace();
	  return "";

	}
  }

  /**
   * Process file depends on environment variable Spring Profiles Activate
   *
   * @param multipartFile
   * @return absolute file path (URL or filesystem)
   */
  public String processFile(MultipartFile multipartFile) {

	if (env.getActiveProfiles().length == 0 || "production".equals(env.getActiveProfiles()[0])) {

	  return (serviceConfig.isEnableTinyCommpresion()) ?
			  amazonClient.uploadFile(multipartFile, BucketRestController.AMAZON_BUCKET_DIR, this) :
			  amazonClient.uploadFile(multipartFile, BucketRestController.AMAZON_BUCKET_DIR);

	} else {

	  try {

		String file = serviceConfig.getTmpPath() + multipartFile.getOriginalFilename();
		File newFile = new File(file);
		multipartFile.transferTo(newFile);

		if (serviceConfig.isEnableTinyCommpresion()) compressAndScale(newFile);

		return file;

	  } catch (IOException e) {
		e.printStackTrace();
		log.error("Problem save file {}", serviceConfig.getTmpPath() + multipartFile.getOriginalFilename());
	  }

	  return "";
	}
  }

  /**
   * Compressing and scale source file. Process will change source file
   *
   * @param fromFile - locale filesystem source
   * @return compressed source file
   */
  public boolean compressAndScale(File fromFile) {

	try {

	  Source source = Tinify.fromFile(fromFile.getAbsolutePath());
	  Options options = new Options()
			  .with("method", "scale")
			  .with("height", 200);
	  source = source.resize(options);
	  source.toFile(fromFile.getAbsolutePath());
	  return true;

	} catch (IOException e) {

	  e.printStackTrace();
	  log.error("Error has been occurred in compression of file {}", fromFile.getAbsolutePath());
	}

	return false;
  }

  /**
   * Compressing source file. Process will change source file
   *
   * @param url - amazon web bucket source
   * @return compressed source file
   */
  public boolean compress(String url) {

	try {

	  Source source = Tinify.fromUrl(url);
	  source.toFile(url);
	  return true;

	} catch (IOException e) {

	  e.printStackTrace();
	  log.error("Error has been occurred in compression of url {}", url);
	}

	return false;
  }

  private String ocr(MultipartFile multipartFile) {
	return ocr(processFile(multipartFile));
  }

  private String getText(MultipartFile multipartFile) {

	if (env.getActiveProfiles().length == 0 || "production".equals(env.getActiveProfiles()[0])) {
	  String url = amazonClient.uploadFile(multipartFile, BucketRestController.AMAZON_BUCKET_DIR);
	  return getUrlContents(url);

	} else {

	  try {
		multipartFile.transferTo(new File(serviceConfig.getTmpPath() + multipartFile.getOriginalFilename()));
		return new String(Files.readAllBytes(
				Paths.get(serviceConfig.getTmpPath() + multipartFile.getOriginalFilename()))
		);
	  } catch (IOException e) {
		e.printStackTrace();
	  }
	}

	return "";

  }
}
