$("#marked-text").mouseover(function () {

    var t = '';

    function gText() {
        t = (document.all) ? document.selection.createRange().text : document.getSelection();
        if (!t.isCollapsed) {
            document.getElementById('newOrigin').value = t;
            var newOrigin = $('#newOrigin');
            var data = {origin: newOrigin.val()};
            $.ajax({
                url: '/translation',
                type: 'POST',
                data: JSON.stringify(data),
                dataType: "json",
                contentType: 'application/json',
                processData: false,   //To avoid making query String instead of JSON
                success: function (data) {
                    if (data) {
                        $('.new-translation').show();
                        $('#newTranscript').val(data.transcript);
                        $('#newNote').val(data.note);
                        $('#newId').val(data.id);
                    } else {
                        $('#newTranscript').val('');
                        $('#newNote').val('');
                        $('#newId').val('NEW');
                    }
                }
            });

        }
    }

    document.onmouseup = gText;
    //if (!document.all) document.captureEvents(Even.MOUSEUP);
});

function saveNew(id) {
    var translation = {
        id: $('input#newId').val(),
        origin: $('input#newOrigin').val(),
        transcript: $('input#newTranscript').val(),
        note: $('input#newNote').val(),
        evaluation: $('input#newEvaluation').val(),
        script: {id: id}
    };
    $.ajax({
        url: '/translation/change',
        type: 'POST',
        dataType: "json",
        contentType: 'application/json',
        data: JSON.stringify(translation),
        processData: false,   //To avoid making query String instead of JSON
        success: function (data) {
            console.log(data);
            $('#newOrigin').val('');
            $('#newTranscript').val('');
            $('#newNote').val('');
            $('.new-translation').hide()
        }
    });
    $('#newOrigin').val('');
    $('#newTranscript').val('');
    $('#newNote').val('');
}

function save(id) {
    save(id, $('#evaluation' + id).val(), false);
}

function remove(id) {
    $.ajax({
        url: '/translation/' + id,
        type: 'DELETE',
        success: function () {
            $('#row_' + id).remove();
        }
    });
}

function save(id, eval, changeStyle) {

    var translation = {
        id: id,
        origin: $('input#origin' + id).val(),
        transcript: $('input#transcript' + id).val(),
        note: $('input#note' + id).val(),
        evaluation: eval
    };

    if (changeStyle) {

        var buttons = [$('#NEW' + id), $('#LEARNING' + id), $('#KNOWN' + id), $('#IGNORE' + id)];

        for (var i = 0; i < buttons.length; i++) {
            buttons[i].removeClass("checked").addClass("default");
        }
        $('#' + eval + id).removeClass("default").addClass("checked");
        showOrigin(id);
        if (eval === 'IGNORE') {
            $('#row_' + id).hide();
        }

    }

    $.ajax({
        url: '/translation/change',
        type: 'POST',
        dataType: "json",
        contentType: 'application/json',
        data: JSON.stringify(translation),
        processData: false,   //To avoid making query String instead of JSON
        success: function (data) {
            console.log(data);
        }
    });
}

function showOrigin(id) {
    var row = $('#row_' + id + ' .origin');
    row.show();
    row.removeClass('d-none');
}

function saveScript(id) {

    var script = {
        id: id,
        title: $('#title').val(),
        content: $('#text').val(),
        embedUrl: $('#embedUrl').val(),
        level: $('#level').find(":selected").val()
    };

    $.ajax({
        url: '/script/change',
        type: 'POST',
        dataType: "json",
        contentType: 'application/json',
        cache: false,
        data: JSON.stringify(script),
        processData: false,   //To avoid making query String instead of JSON
        success: function (data) {
            console.log(data);
            $('#message-text').append("Script has been saved!");
            $('#message').removeClass("hidden").addClass("show");
        },
        error: AjaxFailed
    });

    function AjaxFailed(result) {
        alert(result.status + ' ' + result.statusText);
    }
}

function uploadImageToScript(id) {

    var data = new FormData($('form')[0]);

    $.ajax({
        url: '/script/image/' + id,
        data: data,
        type: 'POST',
        cache: false,
        contentType: false,
        processData: false,   //To avoid making query String instead of JSON
        success: function (data) {
            $('#media').attr('src', data);
        }
    });
}

function onReview() {
    $('#review').val(true);
    $('form').submit();
}

function switchCorpus(target) {
    var chosen = $(target).find(":selected").val();
    var redirect = $(target).data('redirect');
    redirect = redirect.replace('{tran}', chosen);
    location.href = redirect;
}

// ------ text pagination ----------------------------------------
// String.prototype.replaceAll = function(search, replacement) {
//     var target = this;
//     return target.split(search).join(replacement);
// };

// var marketText = $('#marked-text').html();
// var words = marketText.split(" ");
//
// function getText(countOfWords, idx){
//     var result = "";
//     var offset = idx * countOfWords;
//     for (var i=offset; i< offset + countOfWords; i++){
//         result = result + words[i] + " ";
//     }
//     result = result.replaceAll("   ", "");
//     result = result.replaceAll(",", ", ");
//     result = result.replaceAll(".", ". ");
//
//     return result;
// }


function segmentMarketText() {
    var idx = 0;
    var tmp_marked_text = $('#marked-text p');
    var prev = $('#prev');
    var next = $('#next');
    var counter = $('#counter');
    prev.attr('disabled', true);
    var text = [];

    var n = 0;

    $.each(tmp_marked_text, function (key, value) {
        if (key === 0) {
            $('#marked-text').html(value.innerHTML);
        }

        var obj = $.parseHTML(value.innerHTML);
        if (obj.length > 0 && obj[0].innerHTML !== "") {
            text[n] = value.innerHTML;
            n++;
        }
    });


    if (n <= 1) {
        next.attr('disabled', true);
        n = 1;
    }

    counter.text('[ 1' + ' / ' + n + ' ]');
//$('#marked-text').html(getText(20, 0));

    prev.click(function () {
        idx--;
        //$('#marked-text').html(getText(20, idx));
        $('#marked-text').html(text[idx]);
        if (idx === 0) {
            prev.attr('disabled', true);
        }
        next.attr('disabled', false);
        counter.text('[ ' + (idx + 1) + ' / ' + n + ' ]');
    });
    next.click(function () {
        idx++;
        //$('#marked-text').html(getText(20, idx));
        $('#marked-text').html(text[idx]);
        if (idx === n - 1) {
            next.attr('disabled', true);
        }
        prev.attr('disabled', false);
        counter.text('[ ' + (idx + 1) + ' / ' + n + ' ]');
    });
}

// segmentation marked text by paragraph only if video is not included
var isVideo = $('input[name=isVideo]').val();
if (eval(isVideo) === true)
    segmentMarketText();


$(document).ready(function () {

    // -- toggling --------------------
    $('a.toggle-vis').on('click', function (e) {
        e.preventDefault();
        var itemclass = $(this).attr('data-column');
        $('.' + itemclass).toggle();
    });

    $('button.toogle-new-translation').on('click', function (e) {
        e.preventDefault();
        var itemclass = $(this).attr('data-column');
        $('.' + itemclass).toggle();
        $(this).hide();
    });

    // -- translation paging ----------
    var page = 1;
    var n = parseInt($("#pageMax").val());
    var prevT = $(".prevT");
    var nextT = $(".nextT");
    var counterT = $(".counterT");
    prevT.attr('disabled', true);

    prevT.click(function (event) {
        page--;
        var url = $(this).data('href') + page;

        if (page === 1)
            $(this).attr('disabled', true);

        nextT.attr('disabled', false);
        $('.list').load(url);
        counterT.text("[ " + page + " / " + n + " ]");
        event.preventDefault();
    });

    nextT.click(function (event) {

        page++;
        var url = $(this).data('href') + page;

        if (page === n)
            $(this).attr('disabled', true);

        prevT.attr('disabled', false);
        $('.list').load(url);
        counterT.text("[ " + page + " / " + n + " ]");
        event.preventDefault();
    });

    // on change do action
    $('.on-change-action').on('change', function () {
        $(this).closest('form').submit();
    });

    // icons
    feather.replace()
});


