package cucumber;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;


/**
 * @author ⌐⨀_⨀¬ morientes on 23/11/2018.
 */
public class LoginSteps {


  @Given("^user login correctly by username and password$")
  public void user_login_correctly_by_username_and_password() throws Throwable {
	// Write code here that turns the phrase above into concrete actions
	throw new PendingException();
  }

  @When("^user input  \"([^\"]*)\" and \"([^\"]*)\"$")
  public void user_input_and(String arg1, String arg2) throws Throwable {
	// Write code here that turns the phrase above into concrete actions
	throw new PendingException();
  }

  @Then("^redirect to scripts page$")
  public void redirect_to_scripts_page() throws Throwable {
	// Write code here that turns the phrase above into concrete actions
	throw new PendingException();
  }

  @Given("^user login incorrectly by username and password$")
  public void user_login_incorrectly_by_username_and_password() throws Throwable {
	// Write code here that turns the phrase above into concrete actions
	throw new PendingException();
  }

  @Then("^return errors$")
  public void return_errors() throws Throwable {
	// Write code here that turns the phrase above into concrete actions
	throw new PendingException();
  }


}
