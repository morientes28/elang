package sk.morione.elango;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;


/**
 * @author ⌐⨀_⨀¬ morientes on 23/11/2018.
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/cucumber",
		glue = {"src/test/java/cucumber"})
public class CucumberTestRunner {


}
