package sk.morione.elango;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @author ⌐⨀_⨀¬ morientes on 2018-11-23.
 */
@RunWith(SpringRunner.class)
@SpringBootTest()
public class IntegrationTestsRunner /*extends FluentTest*/ {

  //private WebDriver webDriver = new PhantomJSDriver();

  @Value("${local.server.port}")
  private int serverPort;

  //@Override
  // public WebDriver getDefaultDriver() {
  //return webDriver;
  // }

  private String getUrl() {
	return "http://localhost:" + serverPort;
  }

  @Test
  public void hasHeader() {
//	goTo(getUrl()+"/login");
	//assertThat($("h1").getText()).isEqualTo("eLango");
	//assertThat(find(".page-header").getText()).isEqualTo("A checklist");
  }
}

