package sk.morione.elango.access.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import sk.morione.elango.access.model.User;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 28/10/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryTest {

  @Autowired
  private UserRepository userRepository;

  private User user;

  @Before
  public void setUp() {

	user = new User("peto", "heslo", null, null);
	user = userRepository.save(user);
  }

  @Test
  public void findByUsername() {
	User user2 = userRepository.findByUsername("peto");
	Assert.assertEquals(user, user2);

  }

}