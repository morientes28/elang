package sk.morione.elango.core.config;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sk.morione.elango.access.repository.UserRepository;
import sk.morione.elango.script.repository.CorpusRepository;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 11/10/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class LoadDatabaseTest {

  @Autowired
  CommandLineRunner beans;

  @Autowired
  CorpusRepository corpusRepository;

  @Autowired
  UserRepository userRepository;

  @Test
  public void loadBeanFromContext() {
	Assert.assertNotNull(beans);
  }
}