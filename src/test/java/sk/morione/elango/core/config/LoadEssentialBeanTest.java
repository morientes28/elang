package sk.morione.elango.core.config;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.model.Language;
import sk.morione.elango.script.model.Translation;
import sk.morione.elango.script.repository.CorpusRepository;
import sk.morione.elango.script.repository.TranslationRepository;

import java.util.Set;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 08/10/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class LoadEssentialBeanTest {

  @Autowired
  CorpusRepository repository;

  @Autowired
  TranslationRepository translationRepository;

  @Test
  public void loadBeanFromContext() {

	Corpus corpus = repository.findOneByLanguageAndBaseAndName(
			Language.EN, false, "my");
	Assert.assertNotNull(corpus);
	Set<Translation> translation = translationRepository.findByOriginAndCorpus(
			"I", corpus);
	Assert.assertNotNull(translation);

  }

}