package sk.morione.elango.core.job;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.test.context.junit4.SpringRunner;
import sk.morione.elango.core.config.ServiceConfig;
import sk.morione.elango.core.jobs.SchedulerTask;
import sk.morione.elango.core.model.Scheduler;
import sk.morione.elango.core.repository.SchedulerRepository;
import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.model.Language;
import sk.morione.elango.script.model.Script;
import sk.morione.elango.script.model.Translation;
import sk.morione.elango.script.repository.ScriptRepository;
import sk.morione.elango.script.repository.TranslationRepository;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.atLeast;
import static org.mockito.Mockito.verify;


/**
 * Created by  ⌐⨀_⨀¬ morientes on 15/10/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class SchedulerTaskTest {

  @Autowired
  ServiceConfig config;

  @Autowired
  SchedulerRepository repository;

  @Autowired
  TranslationRepository translationRepository;

  @Autowired
  ScriptRepository scriptRepository;

  private Scheduler scheduler;
  private Script script;

  @SpyBean
  private SchedulerTask schedulerTask;

  @Before
  public void before() {
	setTestValues();
  }

  @After
  public void after() {
	// repository.deleteAll();
	// translationRepository.deleteAll();
  }

  @Test
  public void jobRuns() throws InterruptedException {
	Thread.sleep(config.getSchedulerInterval() * 2);
	verify(schedulerTask, atLeast(1)).makeTranslation();
  }

  @Test
  public void testSchedulerTask() {

	schedulerTask.makeTranslation();
	Translation check = translationRepository.findByOriginAndCorpus(
			"have", scheduler.getCorpus()).iterator().next();
	Assert.assertNotNull(check.getOrigin());


//    Pageable one = PageRequest.of(0, 1, Sort.by("id"));
//    Script check2 = scriptRepository.findAllByTitle(
//            "not only", one).iterator().next();
	Assert.assertTrue(script.isProcessed());
  }

  private void setTestValues() {
	String context = "We have not only just";
	script = new Script(context);
	Corpus corpus = new Corpus(Language.EN, "test");
	Set<String> words = new HashSet<>(Arrays.asList(context.split(" ")));
	this.scheduler = new Scheduler(script, corpus, words);
	this.scheduler = repository.save(scheduler);
  }

}