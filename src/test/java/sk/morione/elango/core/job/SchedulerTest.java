package sk.morione.elango.core.job;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;
import sk.morione.elango.core.model.Scheduler;
import sk.morione.elango.core.repository.SchedulerRepository;
import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.model.Language;
import sk.morione.elango.script.model.Script;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 15/10/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class SchedulerTest {

  @Autowired
  SchedulerRepository repository;

  private Scheduler scheduler;
  private Script script;

  @Before
  public void setUp() {
	Corpus corpus = new Corpus(Language.SK, "slovensky");
	script = new Script("Tak toto je testovaci script");
	Set<String> words = new HashSet<>(Arrays.asList("Tak", "toto", "je", "testovaci", "script"));

	scheduler = new Scheduler(script, corpus, words);
  }

  @Test
  public void testConstructor() {

	Assert.assertEquals(script.getContent().length(), scheduler.getWords().length());
	long count = scheduler.getWords().chars().filter(ch -> ch == ',').count();
	Assert.assertEquals(4, count);
  }

  @Test
  public void testRepository() {
	repository.save(scheduler);

	Pageable topTen = PageRequest.of(0, 10, Sort.by("id"));
	Scheduler scheduler1 = repository.findAll(topTen).iterator().next();
	Assert.assertEquals(script.getContent().length(), scheduler1.getWords().length());
	Assert.assertEquals(5, scheduler1.getCollectionWords().size());
  }

}