package sk.morione.elango.script.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import sk.morione.elango.script.model.Language;
import sk.morione.elango.script.repository.CorpusRepository;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static sk.morione.elango.core.config.LoadEssentialBean.BASE_CORPUS_ENG;


/**
 * Created by  ⌐⨀_⨀¬ morientes on 02/10/2018.
 */
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class TranslationControllerTest {

  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private CorpusRepository corpusRepository;

  @Before
  public void setUp() {
	corpusRepository.findOneByLanguageAndBaseAndName(
			Language.EN, true, BASE_CORPUS_ENG);
  }

  @WithMockUser
  @Test
  public void allReturnsCorrectMediaType() throws Exception {
	mockMvc
			.perform(get("/translation"))
			.andExpect(status().isOk())
			.andExpect(content().contentType("text/html;charset=UTF-8"))
			.andExpect(view().name("translation"));
  }

}