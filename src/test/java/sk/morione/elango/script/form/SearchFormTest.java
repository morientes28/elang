package sk.morione.elango.script.form;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 23/10/2018.
 */
public class SearchFormTest {

  SearchForm searchForm;

  @Before
  public void setUp() {
	searchForm = new SearchForm("/scripts");
	searchForm.setRows(10);
	searchForm.setScript("Test script");
	searchForm.setSortBy("id");
	searchForm.setEvaluations(Arrays.asList(1L, 2L));
  }

  @Test
  public void makeUrlParameters() {
	String result = searchForm.makeUrl(1);
	System.out.println(result);
	Assert.assertTrue(result.contains("script"));
	Assert.assertTrue(result.contains("evaluations=1"));
	Assert.assertTrue(result.contains("review=false"));
	Assert.assertFalse(result.contains("evaluations=0"));
	Assert.assertEquals("/scripts?script=Test script&review=false&rows=10&sortBy=id" +
			"&desc=false&evaluations=1&evaluations=2&page=1", searchForm.makeUrl(1));
  }
}