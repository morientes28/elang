package sk.morione.elango.script.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import sk.morione.elango.script.repository.CorpusRepository;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 30/09/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class CorpusTest {

  @Autowired
  private CorpusRepository corpusRepository;

  @Test
  public void whenCreateCorpus_thenIdIsBiggerThan0() {

	Corpus corpus = new Corpus(Language.EN, "test");
	corpus = corpusRepository.save(corpus);
	Assert.assertNotEquals(0, corpus.getId());

  }

  @Test
  public void createCorpusWithMinimumValue() {

	Corpus corpus = new Corpus(Language.EN, "test");
	Translation translation = new Translation("Dog");
	corpus.addTranslation(translation);
	translation.setCorpus(corpus);
	corpus = corpusRepository.save(corpus);
	Assert.assertEquals("Dog",
			corpus.getTranslations().iterator().next().getOrigin()
	);

  }

  @Test
  public void createCorpusWithAllValues() {

	Corpus corpus = new Corpus(Language.EN, "test");
	Translation translation = new Translation("Overload");
	translation.setEvaluation(Evaluation.LEARNING);
	translation.setTranscript("Vylodenie");
	translation.setTranscriptLanguage(Language.SK);
	translation.setNote("Nejaka poznamka k slovicku");
	translation.setCorpus(corpus);
	corpus.addTranslation(translation);
	corpus = corpusRepository.save(corpus);
	Translation compareTrans = corpus.getTranslations().iterator().next();
	Assert.assertEquals("Overload", compareTrans.getOrigin());
	Assert.assertEquals("Vylodenie", compareTrans.getTranscript());
	Assert.assertEquals(Language.SK, compareTrans.getTranscriptLanguage());

  }
}