package sk.morione.elango.script.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import sk.morione.elango.script.repository.LessonRepository;
import sk.morione.elango.script.repository.ScriptRepository;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class LessonTest {

  private Lesson lesson;

  @Autowired
  private LessonRepository lessonRepository;

  @Autowired
  private ScriptRepository scriptRepository;

  @Test
  public void whenCreateLesson_thenIdIsBiggerThan0() {

	lesson = new Lesson();
	lesson.setName("pokus");
	Script script = new Script();
	script.setAuthor("Janko");
	script.setLanguage(Language.SK);
	script.setSource("http://someurl.com");
	script.setContent("Sudca pre prípravné konanie špecializovaného " +
			"trestného súdu rozhodol o vzatí do väzby 4 osôb, ktoré sú obvinené z obzvlášť " +
			"závažného zločinu úkladnej vraždy a iných trestných činov. Dôvodom väzby je obava " +
			"z možného úteku, ovplyvňovania svedkov a pokračovania v trestných činoch." +
			" U obvineného Z.A. dôvod kolúznej väzby zistený nebol. Okrem obvineného Z.A." +
			" podali zvyšní traja obvinení proti uzneseniu sťažnosť, o ktorej rozhodne" +
			" Najvyšší súd SR. Prokurátor sa vzdal práva na podanie sťažnosti,");
	script.setLesson(lesson);
	lesson.addScript(script);
	lesson = lessonRepository.save(lesson);
	Assert.assertNotEquals(0, lesson.getId());
	Assert.assertEquals("pokus", lesson.getName());

  }

  @Test
  public void getParcialScript() {

	lesson = new Lesson();
	lesson.setName("pokus");
	Script script = new Script();
	script.setContent("Sudca pre prípravné");
	script.setLesson(lesson);
	lesson.addScript(script);

	Script script2 = new Script();
	script2.setAuthor("");
	script2.setContent("Druha strana");
	script2.setPage(2);
	script2.setLesson(lesson);
	lesson.addScript(script2);
	lesson = lessonRepository.save(lesson);
	Assert.assertEquals(2, lesson.getScripts().size());

	Script script1 = scriptRepository.findOneByLessonIdAndPage(lesson.getId(), 2);
	Assert.assertEquals("Druha strana", script1.getContent());
  }
}