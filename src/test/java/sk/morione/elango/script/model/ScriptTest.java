package sk.morione.elango.script.model;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.StringUtils;
import sk.morione.elango.script.repository.ScriptRepository;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class ScriptTest {

  private Script script;
  @Autowired
  private ScriptRepository scriptRepository;

  @Test
  public void whenCreateScript_thenIdIsBiggerThan0() {

	script = new Script();
	script.setAuthor("Janko");
	script.setLanguage(Language.SK);
	script.setSource("http://someurl.com");
	script.setContent("Sudca pre prípravné konanie špecializovaného " +
			"trestného súdu rozhodol o vzatí do väzby 4 osôb, ktoré sú obvinené z obzvlášť " +
			"závažného zločinu úkladnej vraždy a iných trestných činov. Dôvodom väzby je obava " +
			"z možného úteku, ovplyvňovania svedkov a pokračovania v trestných činoch." +
			" U obvineného Z.A. dôvod kolúznej väzby zistený nebol. Okrem obvineného Z.A." +
			" podali zvyšní traja obvinení proti uzneseniu sťažnosť, o ktorej rozhodne" +
			" Najvyšší súd SR. Prokurátor sa vzdal práva na podanie sťažnosti,");
	script = scriptRepository.save(script);
	Assert.assertNotEquals(0, script.getId());

  }

  @Test
  public void testSplitContentWithNotAlphaNumericChars() {
	script = new Script();
	script.setAuthor("Janko");
	script.setLanguage(Language.SK);
	script.setSource("http://someurl.com");
	script.setContent("Sudca pre, prípravné !konanie. špecializovaného " +
			"trestného súdu-rozhodol o vzatí do väzby 4 osôb");
	Assert.assertEquals(12, script.getWordsFromContent().size());
  }

  @Test
  public void testSplitContentWithWordsDuplicity() {
	script = new Script();
	script.setAuthor("Peto");
	script.setLanguage(Language.EN);
	script.setSource("http://someurl.com");
	script.setContent("Sudca pre, prípravné !konanie. špecializovaného " +
			"trestného pre rozhodol prípravné do konanie");
	Assert.assertEquals(8, script.getWordsFromContent().size());
  }

  @Test
  public void testSplitContentWithSpecialChars() {
	script = new Script();
	script.setLanguage(Language.EN);
	script.setContent("He. can’t do it, he is, over-whelm");
	Set<String> checkList = script.getWordsFromContent();
	Assert.assertTrue(checkList.contains("can’t"));
	Assert.assertTrue(checkList.contains("is"));
	Assert.assertTrue(checkList.contains("he"));
	Assert.assertTrue(checkList.contains("over-whelm"));
  }

  @Test
  public void testSplitContentWithCapitalcaseAndUppercaseAndLowcaseWords() {
	script = new Script();
	script.setAuthor("Michal");
	script.setLanguage(Language.SK);
	script.setSource("http://someurl.com");
	script.setContent("Sudca pre, prípravné !konanie. Pre " +
			"Konanie pre rozhodol sudCa do konanie");
	Set<String> result = script.getWordsFromContent();

	Assert.assertEquals(6, result.size());
	Assert.assertTrue(checkIfWordIsInSet("sudca", result));
	Assert.assertTrue(checkIfWordIsInSet("pre", result));
	Assert.assertTrue(checkIfWordIsInSet("prípravné", result));
	Assert.assertTrue(checkIfWordIsInSet("konanie", result));
	Assert.assertTrue(checkIfWordIsInSet("rozhodol", result));
	Assert.assertTrue(checkIfWordIsInSet("do", result));
  }

  @Test
  public void testSplitContentWithoutHtmlTags() {
	script = new Script();
	script.setContent("<p>This &nbsp; is ’ the <b>Test</b> and <ul>" +
			" <li>the first</li><li>the last don't don’t</li></ul></p>");
	Set<String> result = script.getWordsFromContent();

	Assert.assertTrue(checkIfWordIsInSet("test", result));
	Assert.assertTrue(checkIfWordIsInSet("don’t", result));
	Assert.assertTrue(checkIfWordIsInSet("don't", result));
	Assert.assertTrue(checkIfWordIsInSet("first", result));
	Assert.assertFalse(checkIfWordIsInSet("p", result));
	Assert.assertFalse(checkIfWordIsInSet("li", result));
	Assert.assertFalse(checkIfWordIsInSet("ul", result));
	Assert.assertFalse(checkIfWordIsInSet("firstthe", result));
	Assert.assertFalse(checkIfWordIsInSet("&#39;", result));
	Assert.assertFalse(checkIfWordIsInSet("&nbsp;", result));
	Assert.assertFalse(checkIfWordIsInSet("’", result));
  }

  private boolean checkIfWordIsInSet(String word, Set<String> words) {

	for (String tran : words) {
	  if (tran.equals(word))
		return true;
	}
	return false;
  }

  @Test
  public void markering() {
	script = new Script();
	script.setContent(" Sudca pre, (prípravného) prípravné .konanie. špecializovaného " +
			"trestného súdu-rozhodol o vzatí do väzby 4 osôb.");
	Translation translation = new Translation("Sudca");
	translation.setEvaluation(Evaluation.NEW);
	Translation translation2 = new Translation("prípravné");
	translation2.setEvaluation(Evaluation.LEARNING);
	Translation translation3 = new Translation("konanie");
	translation3.setEvaluation(Evaluation.LEARNING);
	Translation translation4 = new Translation("špecializovaného");
	translation4.setEvaluation(Evaluation.LEARNING);
	Translation translation5 = new Translation("vzatí");
	translation5.setEvaluation(Evaluation.KNOWN);
	Translation translation6 = new Translation("väzby");
	translation6.setEvaluation(Evaluation.IGNORE);

	Set<Translation> trs = new HashSet<>(
			Arrays.asList(
					translation, translation2, translation3,
					translation4, translation5, translation6)
	);

	trs.forEach((i) -> i.setScript(script));
	script.setTranslations(trs);
	String compareHtml = script.getMarkedContent();
	Assert.assertTrue(compareHtml.contains("eval-new"));
	Assert.assertTrue(compareHtml.contains("eval-learning"));

	// test match special case not only root of word
	int occurance = StringUtils.countOccurrencesOf(compareHtml, "eval-learning");
	Assert.assertEquals(3, occurance);

  }

  @Test
  public void testMediaUrl() {
	script = new Script("This is new world",
			"World",
			"/User/morientes/Work/elango/src/java/main/resources/public/images/some.jpg");
	Assert.assertEquals("/some.jpg", script.getMedia());
	script.setMedia("https://amazon.s3.com/bucket/some.jpg");
	Assert.assertEquals(
			"https://amazon.s3.com/bucket/some.jpg",
			"https://amazon.s3.com/bucket/some.jpg"
	);
  }

  @Test
  public void testMediaS3Url() {
	script = new Script("This is new world",
			"World",
			"https://s3.eu-west-3.amazonaws.com/e-lango/1539367027882-brit.jpg");
	Assert.assertEquals("https://s3.eu-west-3.amazonaws.com/e-lango/1539367027882-brit.jpg", script.getMedia());
  }

  @Test
  public void checkYoutubeUrl() {
	script = new Script("This is new world");
	script.setEmbedUrl("https://www.youtube.com/embed/wmuj8w6s5f8");
	Assert.assertEquals("https://www.youtube.com/embed/wmuj8w6s5f8", script.getEmbedUrl());

	script.setEmbedUrl("https://www.youtube.com/watch?v=wmuj8w6s5f8");
	Assert.assertEquals("https://www.youtube.com/embed/wmuj8w6s5f8", script.getEmbedUrl());

	script.setEmbedUrl("http://youtu.be/wmuj8w6s5f8");
	Assert.assertEquals("https://www.youtube.com/embed/wmuj8w6s5f8", script.getEmbedUrl());


  }

}