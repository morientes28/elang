package sk.morione.elango.script.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import sk.morione.elango.access.model.User;
import sk.morione.elango.access.repository.UserRepository;
import sk.morione.elango.script.model.*;

import java.util.List;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class CorpusRepositoryTest {

  @Autowired
  private CorpusRepository corpusRepository;

  @Autowired
  private UserRepository userRepository;


  private User user;

  @Before
  public void setUp() {
	Script script = new Script();
	script.setContent("Overload Omaha");
	script.setTitle("Overloading");

	user = new User("test", "heslo", null, null);
	user = userRepository.save(user);
	Corpus corpus = new Corpus(Language.EN, "test");
	corpus.setUser(user);

	Translation translation = new Translation("Overload");
	translation.setEvaluation(Evaluation.LEARNING);
	translation.setTranscript("Vylodenie");
	translation.setTranscriptLanguage(Language.SK);
	translation.setNote("Nejaka poznamka k slovicku");
	translation.setCorpus(corpus);
	translation.setScript(script);

	corpus.addTranslation(translation);
	corpusRepository.save(corpus);
  }

  @Test
  public void findByLanguage() {
	List<Corpus> corpusListByName = corpusRepository.findByLanguage(Language.EN);
	Assert.assertNotNull(corpusListByName);
  }

  @Test
  public void findOneByLangugageAndBaseAndName() {
	Corpus base = new Corpus(Language.EN, "test base");
	base.setBase(true);
	corpusRepository.save(base);
	Corpus corpusBase = corpusRepository.findOneByLanguageAndBaseAndName(
			Language.EN, true, "test base");
	Assert.assertNotNull(corpusBase);
  }

  @Test
  public void testAutosaveScriptInTranslation() {
	Corpus corpus = corpusRepository.findOneByLanguageAndBaseAndName(
			Language.EN, false, "test");
	Script script = corpus.getTranslations().iterator().next().getScript();
	Assert.assertEquals("Overloading", script.getTitle());
  }

  @Test
  public void findOneByUserAndLanguage() {
	Corpus corpus = corpusRepository.findOneByUserAndLanguage(user, Language.EN);
	Assert.assertEquals(user, corpus.getUser());
	Corpus corpus2 = corpusRepository.findOneByUserAndLanguage(user, Language.ES);
	Assert.assertNull(corpus2);
  }

}