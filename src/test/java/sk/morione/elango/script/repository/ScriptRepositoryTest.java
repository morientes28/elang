package sk.morione.elango.script.repository;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import sk.morione.elango.access.model.User;
import sk.morione.elango.access.repository.UserRepository;
import sk.morione.elango.script.model.Corpus;
import sk.morione.elango.script.model.Language;
import sk.morione.elango.script.model.Script;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@RunWith(SpringRunner.class)
@ActiveProfiles(value = "test")
@DataJpaTest
public class ScriptRepositoryTest {

  @Autowired
  private CorpusRepository corpusRepository;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private ScriptRepository scriptRepository;

  private User user;
  private Script scriptPrivateRead;
  private Script scriptPrivateUnread;
  private Script scriptPublicRead;
  private Script scriptPublicUnread;

  @Before
  public void setUp() {

	user = new User("testuvac", "heslo", null, null);
	user = userRepository.save(user);

	User user2 = new User("laco", "heslo", null, null);
	user2 = userRepository.save(user2);

	Corpus corpus = new Corpus(Language.EN, "testoavcne");
	corpus.setUser(user);
	corpusRepository.save(corpus);

	scriptPrivateRead = new Script();
	scriptPrivateRead.setContent("Overload Omaha");
	scriptPrivateRead.setTitle("Overloading");
	scriptPrivateRead.addUser(user);
	scriptPrivateRead.setOwner(user);
	user.addScript(scriptPrivateRead);
	scriptPrivateRead = scriptRepository.save(scriptPrivateRead);

	scriptPrivateUnread = new Script();
	scriptPrivateUnread.setContent("Banska Bystrica is a city in the middle of Slovakia");
	scriptPrivateUnread.setTitle("Banska Bystrica");
	scriptPrivateUnread.setOwner(user);
	scriptPrivateUnread = scriptRepository.save(scriptPrivateUnread);

	scriptPublicRead = new Script();
	scriptPublicRead.setContent("Bratislava is a capital of the Slovakia");
	scriptPublicRead.setTitle("Bratislava");
	user.addScript(scriptPublicRead);
	scriptPublicRead.addUser(user);
	scriptPublicRead = scriptRepository.save(scriptPublicRead);

	scriptPublicUnread = new Script();
	scriptPublicUnread.setContent("Ba is a capital of the Slovakia and the biggest city too");
	scriptPublicUnread.setTitle("Bratislava Unread");
	scriptPublicUnread = scriptRepository.save(scriptPublicUnread);
  }

  @Test
  public void findAllUnreadPrivateByCritery() {
	Page<Script> result = scriptRepository.findAllUnreadPrivate(
			"bystrica",
			Language.EN.getId(),
			user.getId(),
			pageRequest()
	);
	Assert.assertNotNull(result);
	Assert.assertEquals(1, result.getTotalElements());
	Assert.assertEquals(scriptPrivateUnread, result.iterator().next());
  }

  @Test
  public void findAllReadPrivateByCritery() {
	Page<Script> result = scriptRepository.findAllReadPrivate(
			"ove",
			Language.EN.getId(),
			user.getId(),
			pageRequest()
	);
	Assert.assertNotNull(result);
	Assert.assertEquals(1, result.getTotalElements());
	Assert.assertEquals(scriptPrivateRead, result.iterator().next());
  }

  @Test
  public void findAllUnreadPublicByCritery() {
	Page<Script> result = scriptRepository.findAllUnreadPublic(
			"Unread",
			Language.EN.getId(),
			user.getId(),
			pageRequest()
	);
	Assert.assertNotNull(result);
	Assert.assertEquals(1, result.getTotalElements());
	Assert.assertEquals(scriptPublicUnread, result.iterator().next());

  }

  @Test
  public void findAllReadPublicByCritery() {
	Page<Script> result = scriptRepository.findAllReadPublic(
			"Bratislava",
			Language.EN.getId(),
			user.getId(),
			pageRequest()
	);
	Assert.assertNotNull(result);
	Assert.assertEquals(1, result.getTotalElements());
	Assert.assertEquals(scriptPublicRead, result.iterator().next());

  }

  @Test
  public void findAllReadPublicAndPrivateByCritery() {
	Page<Script> result = scriptRepository.findAllReadPublicAndPrivate(
			"",
			Language.EN.getId(),
			user.getId(),
			pageRequest()
	);
	Assert.assertNotNull(result);
	Assert.assertEquals(2, result.getTotalElements());

  }

  @Test
  public void findAllUnReadPublicAndPrivateByCritery() {
	Page<Script> result = scriptRepository.findAllUnreadPublicAndPrivate(
			"",
			Language.EN.getId(),
			user.getId(),
			pageRequest()
	);
	Assert.assertNotNull(result);
	Assert.assertEquals(2, result.getTotalElements());

  }


  private PageRequest pageRequest() {
	return PageRequest.of(0, 1, Sort.by(new Sort.Order(Sort.Direction.ASC, "id")));
  }

}