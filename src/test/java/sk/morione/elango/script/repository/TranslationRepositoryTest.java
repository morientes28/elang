package sk.morione.elango.script.repository;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;
import sk.morione.elango.access.model.User;
import sk.morione.elango.access.repository.UserRepository;
import sk.morione.elango.script.model.*;

import java.util.*;

import static org.junit.Assert.*;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class TranslationRepositoryTest {

  private final List<Tag> tags = new ArrayList<>();
  private Corpus corpus;
  private Script script;
  private User user;
  private Translation translation;
  @Autowired
  private TranslationRepository translationRepository;

  @Autowired
  private UserRepository userRepository;

  @Before
  public void setUp() {

	user = userRepository.save(
			new User("test", "test", null, null));

	corpus = new Corpus(Language.EN, "test");
	corpus.setUser(user);
	script = new Script("test script");
	translation = new Translation("Overload");
	translation.setEvaluation(Evaluation.LEARNING);
	translation.setTranscript("Vylodenie");
	translation.setTranscriptLanguage(Language.SK);
	translation.setNote("Nejaka poznamka k slovicku");
	translation.setCorpus(corpus);
	translation.setScript(script);
	tags.add(new Tag("phrase"));
	tags.add(new Tag("sentense"));
	translation.addTags(tags.get(0), tags.get(1));
	corpus.addTranslation(translation);
	translation = translationRepository.save(translation);
  }

  @Test
  public void findAllByTags() {
	Set<Translation> translationList = translationRepository.findAllByTags(
			Arrays.asList(
					tags.get(0).getName(),
					tags.get(1).getName()
			)
	);
	assertNotNull(translationList);
	assertEquals(1, translationList.size());
  }

  @Test
  public void findAllByNotExistOneTag() {
	Set<Translation> translationList = translationRepository.findAllByTags(
			Collections.singletonList("nieco")
	);

	assertTrue(translationList.isEmpty());
  }

  @Test
  public void findByOrigin() {
	Set<Translation> translationList = translationRepository.findByOrigin("Overload");

	assertEquals(1, translationList.size());
	assertEquals(corpus, translationList.iterator().next().getCorpus());
	assertEquals(script, translationList.iterator().next().getScript());
  }

  @Test
  public void whenFindAllByTags_thenOnlyOneResultAccept() {
	Translation translation2 = new Translation("Movie");
	translation2.setEvaluation(Evaluation.LEARNING);
	translation2.setTranscript("Film");
	translation2.setTranscriptLanguage(Language.SK);
	translation2.setCorpus(corpus);
	translation2.addTags(new Tag("xxx"));
	corpus.addTranslation(translation2);
	translationRepository.save(translation2);

	Set<Translation> translationList = translationRepository.findAllByTags(
			Collections.singletonList(tags.get(0).getName())
	);
	assertNotNull(translationList);
	assertEquals(1, translationList.size());
  }

  @Test
  public void findLastByUser() {
	Translation translation = translationRepository.findLastByUsername(user.getUsername());
	assertNotNull(translation);
	assertEquals(user, translation.getCorpus().getUser());
  }
}