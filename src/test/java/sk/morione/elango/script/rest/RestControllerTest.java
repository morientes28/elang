package sk.morione.elango.script.rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import sk.morione.elango.script.controller.LessonRestController;

import static org.assertj.core.api.Assertions.assertThat;


/**
 * Created by  ⌐⨀_⨀¬ morientes on 02/10/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class RestControllerTest {

  @Autowired
  private TranslationRestController translationRestController;

  @Autowired
  private LessonRestController lessonRestController;

  @Test
  public void translationRestControllerInitializedCorrectly() {
	assertThat(translationRestController).isNotNull();
  }

  @Test
  public void lessonRestControllerInitializedCorrectly() {
	assertThat(lessonRestController).isNotNull();
  }

}