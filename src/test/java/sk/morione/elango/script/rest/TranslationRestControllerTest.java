package sk.morione.elango.script.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import sk.morione.elango.access.model.Role;
import sk.morione.elango.access.model.User;
import sk.morione.elango.access.repository.UserRepository;
import sk.morione.elango.script.model.Script;
import sk.morione.elango.script.model.Translation;
import sk.morione.elango.script.repository.ScriptRepository;
import sk.morione.elango.script.repository.TranslationRepository;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 11/10/2018.
 */
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@SpringBootTest
public class TranslationRestControllerTest {

  private static final String USERNAME = "testuser";
  private static final String PASSWORD = "test";

  @Autowired
  TranslationRepository translationRepository;

  @Autowired
  ScriptRepository scriptRepository;

  @Autowired
  UserRepository userRepository;

  //@Autowired
  private MockMvc mockMvc;
  @Autowired
  private WebApplicationContext context;

  @Before
  public void setup() {

	User user = new User(USERNAME,
			"$2a$04$d3zhV4nEn0yq9ryUQLhfceuJBuzNXMf237rmFE7rexRtz41KznROy",
			null, Role.USER);
	userRepository.saveAndFlush(user);
	mockMvc = MockMvcBuilders
			.webAppContextSetup(context)
			.apply(springSecurity())
			.build();
  }

  @WithMockUser(username = USERNAME, password = PASSWORD)
  @Test
  public void getTranslation() throws Exception {

	Translation text = new Translation("testicek");
	ObjectMapper objectMapper = new ObjectMapper();
	mockMvc.perform(post("/translation")
			.contentType(MediaType.APPLICATION_JSON)
			.content(objectMapper.writeValueAsString(text)))
			// NOTE: temporarily
			.andExpect(status().is4xxClientError());
	// FIXME:
	//      .andExpect(status().isOk())
	//      .andExpect(content().contentType("application/json;charset=UTF-8"));
  }

  @WithMockUser
  @Test
  public void changeTranslation() throws Exception {

	Translation text = new Translation("testicek");
	Script script = new Script("okej dog");
	text.setScript(script);
	translationRepository.save(text);
	ObjectMapper objectMapper = new ObjectMapper();
	mockMvc.perform(post("/translation/change")
			.contentType(MediaType.APPLICATION_JSON)
			.content(objectMapper.writeValueAsString(text)))
			// NOTE: temporarily
			.andExpect(status().is4xxClientError());
	// FIXME:
	//     .andExpect(status().isOk())
	//     .andExpect(content().contentType("application/json;charset=UTF-8"))
	//     .andExpect(content().string("{\"response\":\"ok\"}"));
  }

  @WithMockUser
  @Test
  public void deleteTranslation() throws Exception {

	Translation translation = new Translation("okej dog");
	translation = translationRepository.save(translation);

	mockMvc.perform(delete("/translation/" + translation.getId())
			.contentType(MediaType.APPLICATION_JSON))
			// NOTE: temporarily
			.andExpect(status().is4xxClientError());
	// FIXME:
	//      .andExpect(status().isOk())
	//      .andExpect(content().contentType("application/json;charset=UTF-8"))
	//      .andExpect(content().string("{\"response\":\"ok\"}"));


  }


}