package sk.morione.elango.script.service;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import sk.morione.elango.script.model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;


/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
public class MatcherServiceTest {

  private Script script;
  private Corpus corpus;
  private MatcherService service = new MatcherService();

  @Before
  public void before() {
	setLesson();
	setCorpus();
  }

  @Test
  public void matchUknownWords() {
	Set<String> result = service.findUnknownWords(script, corpus);
	Assert.assertNotNull(result);
	Assert.assertFalse(result.isEmpty());
	Assert.assertEquals(22, result.size());
  }

  @Test
  public void matchUknownWordsExceptCitiesAndNames() {
	script.setContent("I would like to know Peter better. He lives in Paris and he often " +
			"travels to Moscow and Berlin.");
	Set<String> result = service.findUnknownWords(script, corpus);
	Assert.assertNotNull(result);
	Assert.assertFalse(result.isEmpty());
	Assert.assertEquals(12, result.size());
  }

  private void setLesson() {
	this.script = new Script();
	this.script.setContent("We have not only just separated our configuration," +
			" but we have transformed it into a Java class! This means we can just " +
			"code with it! Here some examples and ideas");
  }

  private void setCorpus() {
	corpus = new Corpus(Language.EN, "test");
	List<Translation> translationList = new ArrayList<>();

	Translation translation = new Translation("we");
	translation.setId(1);
	translation.setEvaluation(Evaluation.KNOWN);
	translation.setTranscript("my");
	translation.setTranscriptLanguage(Language.SK);
	translation.setCorpus(corpus);
	translationList.add(translation);

	translation = new Translation("have");
	translation.setId(2);
	translation.setEvaluation(Evaluation.KNOWN);
	translation.setTranscript("mať");
	translation.setTranscriptLanguage(Language.SK);
	translation.setCorpus(corpus);
	translationList.add(translation);

	translation = new Translation("configuration");
	translation.setId(3);
	translation.setEvaluation(Evaluation.KNOWN);
	translation.setTranscript("konfigurácia");
	translation.setTranscriptLanguage(Language.SK);
	translation.setCorpus(corpus);
	translationList.add(translation);

	corpus.addTranslations(translationList);

  }
}