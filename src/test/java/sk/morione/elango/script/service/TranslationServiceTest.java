package sk.morione.elango.script.service;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;
import sk.morione.elango.access.model.User;
import sk.morione.elango.access.repository.UserRepository;
import sk.morione.elango.script.form.ImportForm;
import sk.morione.elango.script.model.*;
import sk.morione.elango.script.repository.CorpusRepository;
import sk.morione.elango.script.repository.ScriptRepository;
import sk.morione.elango.script.repository.TranslationRepository;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by  ⌐⨀_⨀¬ morientes on 11/10/2018.
 */
@RunWith(SpringRunner.class)
@DataJpaTest
public class TranslationServiceTest {

  @Autowired
  TranslationService service;

  @Autowired
  ScriptRepository scriptRepository;

  @Autowired
  UserRepository userRepository;

  @Autowired
  CorpusRepository repository;

  @Autowired
  TranslationRepository translationRepository;

  private List<Translation> translationSet = new ArrayList<>();
  private Corpus corpus;
  private User user;

  @Before
  public void setUp() {

	Translation translation = new Translation("hero");
	translationSet.add(translation);
	Translation translation2 = new Translation("is");
	translationSet.add(translation2);
	Translation translation3 = new Translation("my");
	translationSet.add(translation3);
	Translation translation4 = new Translation("wild");
	translationSet.add(translation4);
	Translation translation5 = new Translation("dog");
	translationSet.add(translation5);

	corpus = repository.findOneByLanguageAndBaseAndName(
			Language.EN, false, "my");

	user = new User();
	user.setUsername("test");
	user = userRepository.save(user);
	corpus.setUser(user);

  }

  @Test
  public void randomTransationTest() {

	Assert.assertEquals(4, service.randomTransation(4, new PageImpl<>(translationSet)).getTotalElements());
	Assert.assertEquals(2, service.randomTransation(2, new PageImpl<>(translationSet)).getTotalElements());
	Assert.assertEquals(1, service.randomTransation(1, new PageImpl<>(translationSet)).getTotalElements());
	Assert.assertEquals(4, service.randomTransation(4, new PageImpl<>(translationSet)).getTotalElements());
	Assert.assertEquals(5, service.randomTransation(5, new PageImpl<>(translationSet)).getTotalElements());
  }

  @Test
  public void saveScriptAndTranslation() {
	ImportForm form = new ImportForm();
	form.setTitle("oleee");
	form.setEditordata("test");
	form.setLevel(Level.ELEMENTARY.getId());
	form.setEmbedUrl("https://www.youtube.com/watch?v=wmuj8w6s5f8");
	form.setUserId(user.getId());

	service.saveScript("One day fly I will fly away fly.",
			corpus,
			form,
			null
	);

	Script findScript = scriptRepository.findAllReadPrivate(
			"oleee",
			Language.EN.getId(),
			user.getId(),
			PageRequest.of(0, 1)).iterator().next();
	Assert.assertNotNull(findScript.getTitle());

  }

  @Test
  public void deleteScriptById() {

  }

  @Test
  public void getScriptById() {

  }

  @Test
  public void googleTranslate() {

  }
}