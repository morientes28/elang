package sk.morione.elango.storage.service;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;
import sk.morione.elango.storage.rest.BucketRestController;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 11/10/2018.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class AmazonClientTest {

  @Autowired
  AmazonClient client;

  @Autowired
  FileProcessService fileProcessService;

  private MultipartFile multipartFile;
  private MultipartFile multipartFileImage;

  @Before
  public void setUp() throws IOException {
	File file = new File("src/test/resources/script.txt");
	FileInputStream input = new FileInputStream(file);
	multipartFile = new MockMultipartFile("file",
			file.getName(), "text/plain", IOUtils.toByteArray(input));

	File file2 = new File("src/test/resources/test.png");
	FileInputStream input2 = new FileInputStream(file2);
	multipartFileImage = new MockMultipartFile("file",
			file2.getName(), "image/png", IOUtils.toByteArray(input2));
  }

  @Test
  public void uploadAndDeleteFile() {
	String urlUploadedFile = client.uploadFile(multipartFile, "test");
	Assert.assertNotNull(urlUploadedFile);

	String result = client.deleteFileFromS3Bucket(urlUploadedFile);
	Assert.assertEquals("Successfully deleted", result);
  }

  @Test
  public void testCompressionAmazonFile() {
	long size = multipartFileImage.getSize();
	System.out.println("Size before compression : " + size);
	String url = client.uploadFile(multipartFileImage, BucketRestController.AMAZON_BUCKET_DIR, fileProcessService);
	File newfile = FileProcessService.getUrlFile(url, "/tmp/test.png");
	long size2 = newfile.length();
	System.out.println("Size after compression : " + size2);
	Assert.assertNotEquals(size, size2);
	newfile.deleteOnExit();
  }
}