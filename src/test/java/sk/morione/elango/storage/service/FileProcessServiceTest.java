package sk.morione.elango.storage.service;


import org.apache.commons.io.IOUtils;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.multipart.MultipartFile;
import sk.morione.elango.script.model.Script;

import javax.transaction.Transactional;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Paths;

import static org.apache.http.entity.ContentType.IMAGE_PNG;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by  ⌐⨀_⨀¬ morientes on 01/10/2018.
 */
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@Transactional
@SpringBootTest
public class FileProcessServiceTest {

  @Autowired
  FileProcessService service;

  @Autowired
  private MockMvc mockMvc;

  private String imagePath;
  private MultipartFile multipartFile;
  private MultipartFile multipartFileImage;
  private String newFile;

  @Before
  public void before() throws Exception {
	this.imagePath = Paths.get("src", "test", "resources").toString();
	File file = new File("src/test/resources/script.txt");
	FileInputStream input = new FileInputStream(file);
	multipartFile = new MockMultipartFile("file",
			file.getName(), "text/plain", IOUtils.toByteArray(input));

	File file2 = new File("src/test/resources/test.png");
	FileInputStream input2 = new FileInputStream(file2);
	multipartFileImage = new MockMultipartFile("file",
			file2.getName(), "image/png", IOUtils.toByteArray(input2));
  }

  @After
  public void after() {
	if (newFile != null) {
	  File tmp = new File(newFile);
	  if (tmp.exists()) {
		tmp.deleteOnExit();
	  }
	}
  }

  @Test
  public void whenOCRImageWithSimpleText_thenCharsMustBeFind() {
	String out = service.ocr(this.imagePath + "/test2.JPG");
	Assert.assertNotEquals("", out);
	Assert.assertTrue(out.contains("SD.KFZ."));
  }

  @Test
  public void extractFromTextFile() {
	String result = service.extractFromFileOrText(multipartFile, null);
	Assert.assertTrue(result.contains("purpose created"));
  }

  @Test
  public void extractFromPngFile() {
	String result = service.extractFromFileOrText(multipartFileImage, null);
	Assert.assertTrue(result.contains("And follow my lead"));
  }

  @Test
  public void extractFromText() {
	String result = service.extractFromFileOrText(null, "This is a dream");
	Assert.assertTrue(result.contains("This is a dream"));
  }

  @Test
  public void processFile() {
	String result = service.processFile(multipartFile);
	Assert.assertTrue(result.contains("script.txt"));
  }

  @Test
  public void compressLocalFile() {
	// create test file
	File newFile = new File(service.processFile(multipartFileImage));
	long sourceLength = newFile.length();
	System.out.println("Size before compression : " + sourceLength);
	boolean ok = service.compressAndScale(newFile);
	System.out.println("Size after compression  : " + newFile.length());
	Assert.assertTrue(ok);
	Assert.assertNotEquals(sourceLength, newFile.length());
  }

  @WithMockUser
  @Test
  public void checkPublicResourceFolder() throws Exception {
	newFile = service.processFile(multipartFileImage);
	Script script = new Script("tmp", "title", newFile);
	mockMvc.perform(get(script.getMedia()))
			.andExpect(status().isOk())
			.andExpect(content().contentType(IMAGE_PNG.getMimeType()));
  }
}