Feature: User can login to their account by username - email and password.

  Scenario: User input correct credential
    Given user login correctly by username and password
    When user input  "user" and "test"
    Then redirect to scripts page

  Scenario: User input incorrect credential
    Given user login incorrectly by username and password
    When user input  "userxx" and "testxxx"
    Then return errors
